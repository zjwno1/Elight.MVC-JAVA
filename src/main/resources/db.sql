CREATE TABLE Sys_Item (
  Id varchar(50) NOT NULL COMMENT '主键',
  EnCode varchar(50) DEFAULT NULL COMMENT '编码',
  ParentId varchar(50) DEFAULT NULL,
  Name varchar(50) DEFAULT NULL COMMENT '名称',
  Layer int(11) DEFAULT NULL COMMENT '层次',
  SortCode int(11) DEFAULT NULL COMMENT '排序码',
  IsTree char(1) DEFAULT NULL COMMENT '是否树形菜单',
  DeleteMark char(1) DEFAULT NULL COMMENT '删除标记',
  IsEnabled char(1) DEFAULT NULL COMMENT '是否启用',
  Remark varchar(500) DEFAULT NULL COMMENT '备注',
  CreateUser varchar(50) DEFAULT NULL COMMENT '创建人',
  CreateTime datetime DEFAULT NULL COMMENT '创建时间',
  ModifyUser varchar(50) DEFAULT NULL COMMENT '修改人',
  ModifyTime datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (Id)
);

INSERT INTO Sys_Item VALUES ('0e9a3b52-1cfc-41a4-8f6d-3ed8b321aecf', 'xueli', '8238c495-8376-4004-9a34-56d0dcbd11ea', '学历', '1', '3', null, '0', '1', null, null, null, 'admin', '2017-05-13 19:14:25');
INSERT INTO Sys_Item VALUES ('8238c495-8376-4004-9a34-56d0dcbd11ea', 'all_items', '0', '数据字典', '0', '0', null, '0', '1', null, null, null, null, null);
INSERT INTO Sys_Item VALUES ('9c51a17c-7afd-4986-bfc9-94f9dd818ecf', 'role_type', '8238c495-8376-4004-9a34-56d0dcbd11ea', '角色类型', '1', '1', null, '0', '1', null, null, null, null, null);
INSERT INTO Sys_Item VALUES ('d2f966ba-d541-4ac9-8837-b5303d5c3502', 'org_type', '8238c495-8376-4004-9a34-56d0dcbd11ea', '机构类型', '1', '2', null, '0', '1', null, null, null, null, null);

CREATE TABLE Sys_ItemsDetail (
  Id varchar(50) NOT NULL COMMENT '主键',
  ItemId varchar(50) DEFAULT NULL COMMENT '父级',
  EnCode varchar(50) DEFAULT NULL COMMENT '编码',
  Name varchar(50) DEFAULT NULL COMMENT '选项名称',
  IsDefault char(1) DEFAULT NULL COMMENT '是否默认',
  SortCode int(11) DEFAULT NULL COMMENT '排序码',
  DeleteMark char(1) DEFAULT NULL COMMENT '删除标记',
  IsEnabled char(1) DEFAULT NULL COMMENT '是否启用',
  CreateUser varchar(50) DEFAULT NULL COMMENT '创建人',
  CreateTime datetime DEFAULT NULL COMMENT '创建时间',
  ModifyUser varchar(50) DEFAULT NULL COMMENT '修改人',
  ModifyTime datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (Id)
);

INSERT INTO Sys_ItemsDetail VALUES ('14f0c64a-f3d8-439d-bc0a-d9a5a41a2d46', 'd2f966ba-d541-4ac9-8837-b5303d5c3502', 'org-team', '小组', '0', '4', '0', '1', null, null, 'admin', '2017-07-12 11:00:47');
INSERT INTO Sys_ItemsDetail VALUES ('16c3d367-d63e-4426-9745-ed6824e8454d', '0e9a3b52-1cfc-41a4-8f6d-3ed8b321aecf', 'shuoshi', '硕士', '0', '7', '0', '1', 'admin', '2017-04-29 16:49:54', 'admin', '2017-04-29 16:49:54');
INSERT INTO Sys_ItemsDetail VALUES ('35004B3B-96FB-475D-B3E4-0DD8815D316C', '7b247f60-4095-4ffe-96e0-1935a25852de', 'weihun', '未婚', '0', '1', '0', '1', 'admin', '2017-09-11 15:32:42', 'admin', '2017-09-11 15:32:42');
INSERT INTO Sys_ItemsDetail VALUES ('466142E6-8494-49B0-8E07-03F168D747FE', '7b247f60-4095-4ffe-96e0-1935a25852de', 'yihun', '已婚', '0', '2', '0', '1', 'admin', '2017-09-11 15:32:51', 'admin', '2017-09-11 15:32:51');
INSERT INTO Sys_ItemsDetail VALUES ('557427ff-8bb7-4e8b-ba3d-91f31ab02b59', '0e9a3b52-1cfc-41a4-8f6d-3ed8b321aecf', 'xiaoxue', '小学及以下', '0', '1', '0', '1', 'admin', '2017-04-29 16:44:34', 'admin', '2017-04-29 16:50:15');
INSERT INTO Sys_ItemsDetail VALUES ('738aee95-3597-412e-9a0a-e7e3161c86cf', '9c51a17c-7afd-4986-bfc9-94f9dd818ecf', 'role-business', '业务角色', '1', '2', '0', '1', null, null, 'admin', '2017-06-03 17:38:50');
INSERT INTO Sys_ItemsDetail VALUES ('7c51742f-fed3-48c4-8c5b-7f8b8c64cff0', '0e9a3b52-1cfc-41a4-8f6d-3ed8b321aecf', 'benke', '本科', '1', '5', '0', '1', 'admin', '2017-04-29 16:46:24', 'admin', '2017-04-29 16:50:25');
INSERT INTO Sys_ItemsDetail VALUES ('85d02da8-06f2-4fba-9dcf-7e3b971f9028', 'd2f966ba-d541-4ac9-8837-b5303d5c3502', 'org-company', '公司', '1', '1', '0', '1', null, null, 'admin', '2017-06-03 17:40:04');
INSERT INTO Sys_ItemsDetail VALUES ('85e46a33-b065-4ba2-99da-c02947bfc5e6', 'd2f966ba-d541-4ac9-8837-b5303d5c3502', 'org-department', '部门', '0', '2', '0', '1', null, null, null, null);
INSERT INTO Sys_ItemsDetail VALUES ('ac53424f-adbb-4477-b534-b0bc72ea5f41', '0e9a3b52-1cfc-41a4-8f6d-3ed8b321aecf', 'chuzhong', '初中', '0', '2', '0', '1', 'admin', '2017-04-29 16:44:56', 'admin', '2017-04-29 16:44:56');
INSERT INTO Sys_ItemsDetail VALUES ('C52CBE29-CB92-465F-9697-2AAB7C214FFD', 'd2f966ba-d541-4ac9-8837-b5303d5c3502', 'org-child-dept', '子部门', '0', '3', '0', '1', 'admin', '2017-07-12 11:00:40', 'admin', '2017-07-12 11:00:40');
INSERT INTO Sys_ItemsDetail VALUES ('cb579de4-b816-435f-aaa5-f666a6838ca5', '9c51a17c-7afd-4986-bfc9-94f9dd818ecf', 'role-system', '系统角色', '0', '1', '0', '1', null, null, null, null);
INSERT INTO Sys_ItemsDetail VALUES ('cf5d4197-678f-47b9-8f35-ffc23ba68cee', '9c51a17c-7afd-4986-bfc9-94f9dd818ecf', 'role-other', '其他角色', '0', '3', '0', '1', null, null, null, null);
INSERT INTO Sys_ItemsDetail VALUES ('d327c3ca-a557-4f95-8bbf-659fcf09782d', '0e9a3b52-1cfc-41a4-8f6d-3ed8b321aecf', 'dazhuan', '大专', '0', '4', '0', '1', 'admin', '2017-04-29 16:45:27', 'admin', '2017-04-29 16:45:27');
INSERT INTO Sys_ItemsDetail VALUES ('f500ed63-e91a-40a5-8e80-6b58895007d3', '0e9a3b52-1cfc-41a4-8f6d-3ed8b321aecf', 'yanjiusheng', '研究生', '0', '6', '0', '1', 'admin', '2017-04-29 16:46:45', 'admin', '2017-04-29 16:46:45');
INSERT INTO Sys_ItemsDetail VALUES ('f51b746e-476a-4e39-839f-abed4be676cf', '0e9a3b52-1cfc-41a4-8f6d-3ed8b321aecf', 'gaozhong', '高中', '0', '3', '0', '1', 'admin', '2017-04-29 16:45:06', 'admin', '2017-04-29 16:45:06');
INSERT INTO Sys_ItemsDetail VALUES ('fff309f2-9baa-4283-84a8-74c97fcd83e2', '0e9a3b52-1cfc-41a4-8f6d-3ed8b321aecf', 'boshi', '博士', '0', '8', '0', '1', 'admin', '2017-04-29 16:50:10', 'admin', '2017-09-11 15:32:23');

CREATE TABLE Sys_Log (
  Id varchar(50) NOT NULL COMMENT '主键',
  CreateTime datetime DEFAULT NULL COMMENT '发生时间',
  LogLevel varchar(50) DEFAULT NULL COMMENT '日志等级',
  Operation varchar(50) DEFAULT NULL COMMENT '操作模块',
  Message varchar(500) DEFAULT NULL COMMENT '日志消息',
  Account varchar(50) DEFAULT NULL,
  RealName varchar(50) DEFAULT NULL COMMENT '真实姓名',
  IP varchar(50) DEFAULT NULL COMMENT '操作人IP',
  IPAddress varchar(50) DEFAULT NULL COMMENT '操作人IP归属地',
  Browser varchar(50) DEFAULT NULL,
  StackTrace varchar(500) DEFAULT NULL COMMENT '堆栈信息',
  PRIMARY KEY (Id)
);
CREATE TABLE Sys_Organize (
  Id varchar(50) NOT NULL COMMENT '主键',
  ParentId varchar(50) DEFAULT NULL COMMENT '父级',
  Layer int(11) DEFAULT NULL COMMENT '层次',
  EnCode varchar(50) DEFAULT NULL COMMENT '编码',
  FullName varchar(50) DEFAULT NULL COMMENT '全称',
  Type int(11) DEFAULT NULL COMMENT '分类',
  ManagerId varchar(50) DEFAULT NULL COMMENT '负责人',
  TelePhone varchar(50) DEFAULT NULL COMMENT '固话',
  WeChat varchar(50) DEFAULT NULL COMMENT '微信',
  Fax varchar(50) DEFAULT NULL COMMENT '传真',
  Email varchar(50) DEFAULT NULL COMMENT '邮箱',
  Address varchar(50) DEFAULT NULL COMMENT '地址',
  SortCode int(11) DEFAULT NULL COMMENT '排序码',
  DeleteMark char(1) DEFAULT NULL COMMENT '删除标记',
  IsEnabled char(1) DEFAULT NULL COMMENT '是否启用',
  Remark varchar(500) DEFAULT NULL COMMENT '备注',
  CreateUser varchar(50) DEFAULT NULL COMMENT '创建人',
  CreateTime datetime DEFAULT NULL COMMENT '创建时间',
  ModifyUser varchar(50) DEFAULT NULL COMMENT '修改人',
  ModifyTime datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (Id)
);

INSERT INTO Sys_Organize VALUES ('25fa48f8-00d3-4b5d-bee9-b49324410906', '771b628b-e43c-4592-b1ef-70ea23b0e3f2', null, 'market', '市场部', '1', null, null, null, null, null, null, '20', '0', '1', null, null, null, 'admin', '2018-04-04 11:44:34');
INSERT INTO Sys_Organize VALUES ('339a409a-a5a6-49b4-9071-86d7699a9ddd', '771b628b-e43c-4592-b1ef-70ea23b0e3f2', null, 'administration', '行政人事部', '1', null, null, null, null, null, null, '40', '0', '1', null, null, null, 'admin', '2018-04-04 11:44:43');
INSERT INTO Sys_Organize VALUES ('771b628b-e43c-4592-b1ef-70ea23b0e3f2', '0', null, 'company', '某某公司', '0', '某某', null, null, null, 'aa@qq.com', null, '10', '0', '1', null, null, null, 'admin', '2018-04-04 11:42:33');
INSERT INTO Sys_Organize VALUES ('a93c66e2-b8dc-4d00-84ed-e6071b5f5318', '771b628b-e43c-4592-b1ef-70ea23b0e3f2', null, 'product', '产品事业部', '1', null, null, null, null, null, null, '30', '0', '1', null, null, null, 'admin', '2018-04-04 11:44:11');

CREATE TABLE Sys_Permission (
  Id varchar(50) NOT NULL COMMENT '主键',
  ParentId varchar(50) DEFAULT NULL COMMENT '父级',
  Layer int(11) DEFAULT NULL COMMENT '层次',
  EnCode varchar(50) DEFAULT NULL,
  Name varchar(50) DEFAULT NULL COMMENT '名称',
  JsEvent varchar(50) DEFAULT NULL COMMENT '事件',
  Icon varchar(50) DEFAULT NULL COMMENT '图标',
  Url varchar(300) DEFAULT NULL COMMENT '链接',
  Remark varchar(500) DEFAULT NULL COMMENT '备注',
  Type int(11) DEFAULT NULL COMMENT '模块类型：1-菜单 2-按钮',
  SortCode int(11) DEFAULT NULL COMMENT '排序码',
  IsPublic char(1) DEFAULT '0' COMMENT '是否公开',
  IsEnable char(1) DEFAULT '1' COMMENT '是否可用',
  IsEdit char(1) DEFAULT '1' COMMENT '允许编辑',
  DeleteMark char(1) DEFAULT '0',
  CreateUser varchar(50) DEFAULT NULL COMMENT '创建人',
  CreateTime datetime DEFAULT NULL COMMENT '创建时间',
  ModifyUser varchar(50) DEFAULT NULL COMMENT '修改人',
  ModifyTime datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (Id)
);

INSERT INTO Sys_Permission VALUES ('026550fd-2578-42ae-a041-625cda12325f', '855f3590-b233-4224-aaff-47fb95c8353d', '2', 'role-add', '新增角色', 'btn_add()', 'fa fa-plus-square-o', '/System/Role/Form', null, '1', '10301', '0', '1', '1', '0', 'admin', '2017-03-28 16:30:21', 'admin', '2017-03-28 16:30:21');
INSERT INTO Sys_Permission VALUES ('0285224befb044859da0c0b7c5b5ec11', 'e32b7507-aaf0-42dc-8008-139250c352ee', '3', 'item-edit', '修改字典', 'btn_edit()', 'fa fa-pencil-square-o', '/System/Item/Form', '', '1', '10503', '0', '1', '0', '0', 'admin', '2018-06-16 19:29:12', 'admin', '2018-06-16 19:29:12');
INSERT INTO Sys_Permission VALUES ('069f00f6-2a82-4bbe-90d6-418f37d5ef1f', '3c69e3fb-e1fe-4911-8417-6f6d55a1ce72', '2', 'item-detail-detail', '查看选项', 'btn_detail()', 'fa fa-eye', '/System/ItemsDetail/Detail', '', '1', '10513', '0', '1', '1', '0', 'admin', '2017-04-04 20:16:02', 'admin', '2018-06-16 19:26:47');
INSERT INTO Sys_Permission VALUES ('086ee328-5a15-40b0-8e15-291093e2e8b1', '09157352-1252-4964-8fee-479759a95db8', '2', 'org-edit', '修改机构', 'btn_edit()', 'fa fa-pencil-square-o', '/System/Organize/Form', null, '1', '10402', '0', '1', '1', '0', 'admin', '2017-04-02 09:38:32', 'admin', '2017-04-02 09:38:32');
INSERT INTO Sys_Permission VALUES ('09157352-1252-4964-8fee-479759a95db8', '2d0b02db-09f7-4404-bbdd-c8a516f48288', '1', 'sys-organize', '组织机构', null, 'fa fa-building', '/System/Organize/Index', null, '0', '10400', '0', '1', '1', '0', 'admin', '2017-04-02 09:31:00', 'admin', '2017-09-14 13:56:08');
INSERT INTO Sys_Permission VALUES ('0d2ea3c9-5b29-4bb6-9f91-0322419ded8e', 'e5346fa2-76ec-498f-8f54-3b443959335a', '2', 'per-delete', '删除权限', 'btn_delete()', 'fa fa-trash-o', '/System/Permission/Delete', null, '1', '10203', '0', '1', '1', '0', 'admin', '2017-02-20 09:51:18', 'admin', null);
INSERT INTO Sys_Permission VALUES ('216d09a8-575f-43d1-85f6-acc025fa94b3', '6d90439c-eb6b-4521-ab4d-5e481406a861', '2', 'user-detail', '查看用户', 'btn_detail()', 'fa fa-eye', '/System/User/Detail', null, '1', '10104', '0', '1', '1', '0', 'admin', '2017-03-28 16:20:17', 'admin', '2017-03-28 16:20:17');
INSERT INTO Sys_Permission VALUES ('233e50fd-4860-42f9-aa7a-93853ac0434b', '277c8647-ea81-42cf-8f7b-db353da95bbe', '1', 'data-backup', '数据备份', null, 'fa fa-list', '/System/Data/Index', null, '0', '20100', '0', '1', '1', '0', 'admin', null, 'admin', '2017-07-12 10:50:53');
INSERT INTO Sys_Permission VALUES ('277c8647-ea81-42cf-8f7b-db353da95bbe', '0', '0', null, '系统安全', null, 'fa fa-desktop', null, null, '0', '20000', '0', '1', '1', '0', 'admin', null, 'admin', null);
INSERT INTO Sys_Permission VALUES ('28a045a6-61f4-4784-8578-837ad307e4e3', 'e5346fa2-76ec-498f-8f54-3b443959335a', '2', 'per-add', '新增权限', 'btn_add()', 'fa fa-plus-square-o', '/System/Permission/Form', null, '1', '10201', '0', '1', '1', '0', 'admin', '2017-02-13 14:28:21', 'admin', null);
INSERT INTO Sys_Permission VALUES ('2c24cdfc-8f26-4947-bcb2-0cb4d9111e80', 'e5346fa2-76ec-498f-8f54-3b443959335a', '2', 'per-detail', '查看权限', 'btn_detail()', 'fa fa-eye', '/System/Permission/Detail', null, '1', '10204', '0', '1', '1', '0', 'admin', '2017-03-28 16:22:05', 'admin', '2017-03-28 16:22:05');
INSERT INTO Sys_Permission VALUES ('2d0b02db-09f7-4404-bbdd-c8a516f48288', '0', '0', null, '系统管理', null, 'fa fa-cubes', null, null, '0', '10000', '0', '1', '1', '0', 'admin', null, 'admin', null);
INSERT INTO Sys_Permission VALUES ('328b5383-79be-4b34-b57a-49fa3ebc7803', '855f3590-b233-4224-aaff-47fb95c8353d', '2', 'role-delete', '删除角色', 'btn_delete()', 'fa fa-trash-o', '/System/Role/Delete', null, '1', '10303', '0', '1', '1', '0', 'admin', '2017-03-28 16:32:43', 'admin', '2017-03-28 16:32:43');
INSERT INTO Sys_Permission VALUES ('3c69e3fb-e1fe-4911-8417-6f6d55a1ce72', '2d0b02db-09f7-4404-bbdd-c8a516f48288', '1', 'lay-item', '数据字典', null, 'fa fa-sitemap', '/System/ItemsDetail/Index', null, '0', '10500', '0', '1', '1', '0', 'admin', '2017-04-03 15:33:02', 'admin', '2017-04-03 15:33:02');
INSERT INTO Sys_Permission VALUES ('3de13971-a51f-40f7-be40-eb035b7f0fae', '6d90439c-eb6b-4521-ab4d-5e481406a861', '2', 'user-edit', '修改用户', 'btn_edit()', 'fa fa-edit', '/System/User/Edit', null, '1', '10102', '0', '1', '1', '0', 'admin', '2017-04-14 17:21:43', 'admin', '2017-06-05 10:48:07');
INSERT INTO Sys_Permission VALUES ('55265bc5309c4caab59547542487f1bb', 'e32b7507-aaf0-42dc-8008-139250c352ee', '3', 'item-delete', '删除字典', 'btn_delete()', 'fa fa-trash-o', '/System/Item/Delete', '', '1', '10504', '0', '1', '0', '0', 'admin', '2018-06-16 19:29:59', 'admin', '2018-06-16 19:29:59');
INSERT INTO Sys_Permission VALUES ('5fe0cee6-0452-493d-9b55-ff23a5da5e2d', 'e5346fa2-76ec-498f-8f54-3b443959335a', '2', 'per-edit', '修改权限', 'btn_edit()', 'fa fa-pencil-square-o', '/System/Permission/Form', null, '1', '10202', '0', '1', '1', '0', 'admin', '2017-02-20 09:47:19', 'admin', null);
INSERT INTO Sys_Permission VALUES ('625cf550-4aad-4158-aff4-2a63d4f25819', '855f3590-b233-4224-aaff-47fb95c8353d', '2', 'role-detail', '查看角色', 'btn_detail()', 'fa fa-eye', '/System/Role/Detail', null, '1', '10304', '0', '1', '1', '0', 'admin', '2017-03-28 16:34:05', 'admin', '2017-03-28 16:34:05');
INSERT INTO Sys_Permission VALUES ('6d90439c-eb6b-4521-ab4d-5e481406a861', '2d0b02db-09f7-4404-bbdd-c8a516f48288', '1', 'sys-user', '系统用户', null, 'fa fa-user-circle', '/System/User/Index', null, '0', '10100', '0', '1', '1', '0', 'admin', null, 'admin', '2017-09-14 13:51:29');
INSERT INTO Sys_Permission VALUES ('752c9d3f-a744-42ba-87a2-79849fc3fc66', '6d90439c-eb6b-4521-ab4d-5e481406a861', '2', 'user-delete', '删除用户', 'btn_delete()', 'fa fa-trash-o', '/System/User/Delete', null, '1', '10103', '0', '1', '1', '0', 'admin', '2017-03-28 16:18:25', 'admin', '2017-03-28 16:18:25');
INSERT INTO Sys_Permission VALUES ('7ae2e6aa-0433-4eaa-9357-1adec2507345', 'aeeb56d1-5f27-42df-9d34-97ac18078390', '2', 'log-delete', '删除日志', 'btn_delete()', 'fa fa-trash-o', '/System/Log/Delete', null, '1', '10601', '0', '1', '0', '0', 'admin', '2017-04-19 13:21:33', 'admin', '2017-04-19 13:22:35');
INSERT INTO Sys_Permission VALUES ('81d1cbf0-3cff-4cde-8128-7d0d844450de', '855f3590-b233-4224-aaff-47fb95c8353d', '2', 'role-authorize', '角色授权', 'btn_authorize()', 'fa fa-hand-pointer-o', '/System/RoleAuthorize/Index', null, '1', '10305', '0', '1', '1', '0', 'admin', '2017-03-28 16:36:42', 'admin', '2017-03-28 16:36:42');
INSERT INTO Sys_Permission VALUES ('82b06e80-103e-4a38-b171-740d2b0e194b', '09157352-1252-4964-8fee-479759a95db8', '2', 'org-add', '新增机构', 'btn_add()', 'fa fa-plus-square-o', '/System/Organize/Form', null, '1', '10401', '0', '1', '1', '0', 'admin', '2017-04-02 09:37:47', 'admin', '2017-04-02 09:37:47');
INSERT INTO Sys_Permission VALUES ('85438f3b-0634-4b17-b778-aee3a5819669', '855f3590-b233-4224-aaff-47fb95c8353d', '2', 'role-edit', '修改角色', 'btn_edit()', 'fa fa-pencil-square-o', '/System/Role/Form', null, '1', '10302', '0', '1', '1', '0', 'admin', '2017-03-28 16:31:10', 'admin', '2017-03-28 16:31:10');
INSERT INTO Sys_Permission VALUES ('855f3590-b233-4224-aaff-47fb95c8353d', '2d0b02db-09f7-4404-bbdd-c8a516f48288', '1', 'sys-role', '角色管理', null, 'fa fa-users', '/System/Role/Index', null, '0', '10300', '0', '1', '1', '0', 'admin', '2017-03-28 16:27:50', 'admin', '2017-09-14 13:52:18');
INSERT INTO Sys_Permission VALUES ('87f0aa68-fa57-43cb-84d0-e979fc4af24c', '3c69e3fb-e1fe-4911-8417-6f6d55a1ce72', '2', 'item-detail-delete', '删除选项', 'btn_delete()', 'fa fa-trash-o', '/System/ItemsDetail/Delete', '', '1', '10512', '0', '1', '1', '0', 'admin', '2017-04-04 20:06:34', 'admin', '2018-06-16 19:26:40');
INSERT INTO Sys_Permission VALUES ('aeeb56d1-5f27-42df-9d34-97ac18078390', '2d0b02db-09f7-4404-bbdd-c8a516f48288', '1', 'sys-log', '操作日志', null, 'fa fa-folder-open', '/System/Log/Index', null, '0', '10600', '0', '1', '0', '0', 'admin', '2017-04-18 13:25:49', 'admin', '2017-04-19 13:22:14');
INSERT INTO Sys_Permission VALUES ('c04bfd8f-7e2e-4312-9148-a2e14007fa46', '3c69e3fb-e1fe-4911-8417-6f6d55a1ce72', '2', 'item-detail-edit', '修改选项', 'btn_edit()', 'fa fa-pencil-square-o', '/System/ItemsDetail/Form', '', '1', '10511', '0', '1', '1', '0', 'admin', '2017-04-04 20:05:36', 'admin', '2018-06-16 19:29:01');
INSERT INTO Sys_Permission VALUES ('cd4e9f8b-f56a-42dc-94e1-b76f3d0b38fc', '09157352-1252-4964-8fee-479759a95db8', '2', 'org-detail', '查看机构', 'btn_detail()', 'fa fa-eye', '/System/Organize/Detail', null, '1', '10404', '0', '1', '1', '0', 'admin', '2017-04-02 09:47:18', 'admin', '2017-04-02 09:47:18');
INSERT INTO Sys_Permission VALUES ('d9cfc79d-55f6-4890-b604-49f1d2a0d971', '6d90439c-eb6b-4521-ab4d-5e481406a861', '2', 'user-add', '新增用户', 'btn_add()', 'fa fa-plus-square-o', '/System/User/Form', null, '1', '10101', '0', '1', '1', '0', 'admin', '2017-03-28 16:14:58', 'admin', '2017-03-28 16:14:58');
INSERT INTO Sys_Permission VALUES ('e00ba31fe82143299f4143e505345d83', 'e32b7507-aaf0-42dc-8008-139250c352ee', '3', 'item-add', '新增字典', 'btn_add()', 'fa fa-plus-square-o', '/System/Items/Form', '', '1', '10502', '0', '1', '0', '0', 'admin', '2018-06-16 19:27:55', 'admin', '2018-06-16 19:27:55');
INSERT INTO Sys_Permission VALUES ('e32b7507-aaf0-42dc-8008-139250c352ee', '3c69e3fb-e1fe-4911-8417-6f6d55a1ce72', '2', 'item-manage', '字典管理', 'btn_manage()', 'fa fa-folder-open-o', '/System/Item/Index', null, '1', '10501', '0', '1', '1', '0', 'admin', '2017-04-03 21:30:55', 'admin', '2017-04-04 10:48:52');
INSERT INTO Sys_Permission VALUES ('e5346fa2-76ec-498f-8f54-3b443959335a', '2d0b02db-09f7-4404-bbdd-c8a516f48288', '1', 'sys-permission', '权限管理', null, 'fa fa-suitcase', '/System/Permission/Index', null, '0', '10200', '0', '1', '1', '0', 'admin', null, 'admin', '2017-03-28 16:58:50');
INSERT INTO Sys_Permission VALUES ('e730c012b50141c893f43f903f6854aa', 'e32b7507-aaf0-42dc-8008-139250c352ee', '3', 'item-detail', '查看字典', 'btn_detail()', 'fa fa-eye', '/System/Item/Detail', '', '1', '10505', '0', '1', '0', '0', 'admin', '2018-06-16 19:30:46', 'admin', '2018-06-16 19:30:56');
INSERT INTO Sys_Permission VALUES ('e9478f45-0c00-435f-9a7a-35c7af1f86f7', '09157352-1252-4964-8fee-479759a95db8', '2', 'org-delete', '删除机构', 'btn_delete()', 'fa fa-trash-o', '/System/Organize/Delete', null, '1', '10403', '0', '1', '1', '0', 'admin', '2017-04-02 09:45:55', 'admin', '2017-04-02 09:45:55');
INSERT INTO Sys_Permission VALUES ('fbee5749-8694-495f-b140-b5b3399df7ee', '3c69e3fb-e1fe-4911-8417-6f6d55a1ce72', '2', 'item-detail-add', '新增选项', 'btn_add()', 'fa fa-plus-square-o', '/System/ItemsDetail/Form', '', '1', '10510', '0', '1', '1', '0', 'admin', '2017-04-04 19:46:18', 'admin', '2018-06-16 19:26:23');

CREATE TABLE Sys_Role (
  Id varchar(50) NOT NULL COMMENT '主键',
  OrganizeId varchar(50) DEFAULT NULL COMMENT '组织ID',
  EnCode varchar(50) DEFAULT NULL COMMENT '编号',
  Type smallint(6) DEFAULT NULL COMMENT '分类：1-角色2-岗位',
  Name varchar(50) DEFAULT NULL COMMENT '名称',
  AllowEdit char(1) DEFAULT NULL COMMENT '是否可编辑',
  DeleteMark char(1) DEFAULT NULL COMMENT '删除标记',
  IsEnabled char(1) DEFAULT NULL COMMENT '是否启用',
  Remark varchar(500) DEFAULT NULL COMMENT '备注',
  SortCode int(11) DEFAULT NULL COMMENT '排序码',
  CreateUser varchar(50) DEFAULT NULL COMMENT '创建人',
  CreateTime datetime DEFAULT NULL COMMENT '创建时间',
  ModifyUser varchar(50) DEFAULT NULL COMMENT '修改人',
  ModifyTime datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (Id)
);

INSERT INTO Sys_Role VALUES ('a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '771b628b-e43c-4592-b1ef-70ea23b0e3f2', 'administrators', '0', '超级管理员', '1', '0', '1', null, '1', 'admin', null, 'admin', '2018-04-04 11:44:58');
INSERT INTO Sys_Role VALUES ('db60dc76-9632-44b3-ae4b-7177428bad35', '771b628b-e43c-4592-b1ef-70ea23b0e3f2', 'configuration', '0', '系统配置员', '0', '0', '1', null, '2', 'admin', null, 'admin', '2018-04-04 11:45:31');

CREATE TABLE Sys_RoleAuthorize(
  Id varchar(50) NOT NULL COMMENT '主键',
  RoleId varchar(50) DEFAULT NULL COMMENT '角色ID',
  ModuleId varchar(50) DEFAULT NULL COMMENT '模块ID',
  CreateUser varchar(50) DEFAULT NULL COMMENT '创建人',
  CreateTime datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (Id)
);


INSERT INTO Sys_RoleAuthorize VALUES ('02bbdced04494f3a9f29849202bebda1', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '855f3590-b233-4224-aaff-47fb95c8353d', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('15a5a5ace3544958a07093455a78ccf7', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '026550fd-2578-42ae-a041-625cda12325f', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('165a4b08-4c60-4faf-92ea-3e143aa1e7c4', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '3c69e3fb-e1fe-4911-8417-6f6d55a1ce72', 'admin', '2017-09-14 11:43:13');
INSERT INTO Sys_RoleAuthorize VALUES ('199964680d4e4699a16dc4dbafd3c6bc', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '81d1cbf0-3cff-4cde-8128-7d0d844450de', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('23fad4e1d6b648278011b97dcea939c2', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '0d2ea3c9-5b29-4bb6-9f91-0322419ded8e', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('263bc5c4-5d5c-4592-a115-0f2034553e90', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', 'e9478f45-0c00-435f-9a7a-35c7af1f86f7', 'admin', '2017-09-14 11:43:13');
INSERT INTO Sys_RoleAuthorize VALUES ('28f5d1ab4f094a51949fd00934cbea9a', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '09157352-1252-4964-8fee-479759a95db8', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('2a6694caa2e84fadae6a0e27d78d15a6', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', 'd9cfc79d-55f6-4890-b604-49f1d2a0d971', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('2b9c31814e1441ba9bff9f10c4e533b8', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '086ee328-5a15-40b0-8e15-291093e2e8b1', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('30efcba746ee47ef9fb0f6392aa4edbd', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '6d90439c-eb6b-4521-ab4d-5e481406a861', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('3123c97c0be048c2bc4394ef3851d8ba', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', 'e9478f45-0c00-435f-9a7a-35c7af1f86f7', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('32f73f36aacb43fa9f0cc240841de9dc', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '87f0aa68-fa57-43cb-84d0-e979fc4af24c', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('34da1d8c5b4847f8af4deab9a56701f8', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', 'cd4e9f8b-f56a-42dc-94e1-b76f3d0b38fc', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('3602a15b6a35434c89eb5ed4b43c524b', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '069f00f6-2a82-4bbe-90d6-418f37d5ef1f', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('3b2baa1c-2fda-4620-a3c7-58fd45d87b0a', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '2d0b02db-09f7-4404-bbdd-c8a516f48288', 'admin', '2017-09-14 11:43:12');
INSERT INTO Sys_RoleAuthorize VALUES ('3b4052c3-e846-4cc1-bced-e818342d3e0b', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', 'e32b7507-aaf0-42dc-8008-139250c352ee', 'admin', '2017-09-14 11:43:13');
INSERT INTO Sys_RoleAuthorize VALUES ('3c7f96a622534730b7228be34a7a92a8', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '233e50fd-4860-42f9-aa7a-93853ac0434b', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('40afb27b8d81427eb3e3022d9c6f2045', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '2c24cdfc-8f26-4947-bcb2-0cb4d9111e80', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('414eadbd5bcc42aca3e1995d621df5de', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', 'aeeb56d1-5f27-42df-9d34-97ac18078390', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('431a168046dc453f903d5622fe3f5bc0', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', 'c04bfd8f-7e2e-4312-9148-a2e14007fa46', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('48fd0cd589004527b33ebc1205c7ce25', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '82b06e80-103e-4a38-b171-740d2b0e194b', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('4d24fcca-e1ae-4816-879f-34aa96b93dc2', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '5fe0cee6-0452-493d-9b55-ff23a5da5e2d', 'admin', '2017-09-14 11:43:12');
INSERT INTO Sys_RoleAuthorize VALUES ('4f6d3060cf2d4914aa582b1755f18e87', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', 'e32b7507-aaf0-42dc-8008-139250c352ee', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('5b0801f745f84f6fa712943e1667dd27', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '28a045a6-61f4-4784-8578-837ad307e4e3', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('6058b4642b6747709ea8eeb1d95dc16f', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '216d09a8-575f-43d1-85f6-acc025fa94b3', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('608619a2-fc79-4179-992d-11aef520f8de', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '87f0aa68-fa57-43cb-84d0-e979fc4af24c', 'admin', '2017-09-14 11:43:13');
INSERT INTO Sys_RoleAuthorize VALUES ('648c6f72-2e50-41b7-88ea-6a57efc29102', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '28a045a6-61f4-4784-8578-837ad307e4e3', 'admin', '2017-09-14 11:43:12');
INSERT INTO Sys_RoleAuthorize VALUES ('6ed719e150894e69a82ca5f817b01945', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '0285224befb044859da0c0b7c5b5ec11', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('7224563a-50af-42df-a66b-30e8d41e08fe', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', 'cd4e9f8b-f56a-42dc-94e1-b76f3d0b38fc', 'admin', '2017-09-14 11:43:13');
INSERT INTO Sys_RoleAuthorize VALUES ('7323bcad906441c6a6a2381f337be50d', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', 'e730c012b50141c893f43f903f6854aa', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('7375dea3-ee3d-40cb-8390-9c1cb9baf6a0', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', 'd9cfc79d-55f6-4890-b604-49f1d2a0d971', 'admin', '2017-09-14 11:43:12');
INSERT INTO Sys_RoleAuthorize VALUES ('74d34c58881f4557beec23d9e434d416', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '752c9d3f-a744-42ba-87a2-79849fc3fc66', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('765de5b7-be99-494e-a173-1dd2238ad1f1', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '086ee328-5a15-40b0-8e15-291093e2e8b1', 'admin', '2017-09-14 11:43:13');
INSERT INTO Sys_RoleAuthorize VALUES ('7fdd045d-9de9-466d-a332-7c65028d9b4b', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', 'fbee5749-8694-495f-b140-b5b3399df7ee', 'admin', '2017-09-14 11:43:13');
INSERT INTO Sys_RoleAuthorize VALUES ('858d41fc2913437ca94ba8980cce7500', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '85438f3b-0634-4b17-b778-aee3a5819669', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('892cf221d5364b5ea10484286cc14a2f', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '5fe0cee6-0452-493d-9b55-ff23a5da5e2d', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('8cb47a49-27fe-47c9-818f-0aad37cff810', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '752c9d3f-a744-42ba-87a2-79849fc3fc66', 'admin', '2017-09-14 11:43:12');
INSERT INTO Sys_RoleAuthorize VALUES ('97141b883a8c4946969198fd4120af68', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '55265bc5309c4caab59547542487f1bb', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('98615c60-0066-4f13-9253-70e56b3ec34c', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', 'e5346fa2-76ec-498f-8f54-3b443959335a', 'admin', '2017-09-14 11:43:12');
INSERT INTO Sys_RoleAuthorize VALUES ('99980d3a-ad3b-4c20-9cdd-9f809225badd', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '0d2ea3c9-5b29-4bb6-9f91-0322419ded8e', 'admin', '2017-09-14 11:43:12');
INSERT INTO Sys_RoleAuthorize VALUES ('9b64ef96-d367-4732-a434-cf76640cab05', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '625cf550-4aad-4158-aff4-2a63d4f25819', 'admin', '2017-09-14 11:43:13');
INSERT INTO Sys_RoleAuthorize VALUES ('a0b99832-8425-45ba-b483-248a3cb76a55', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '81d1cbf0-3cff-4cde-8128-7d0d844450de', 'admin', '2017-09-14 11:43:13');
INSERT INTO Sys_RoleAuthorize VALUES ('a3ae756f04b94ac4b23520141969a698', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '3c69e3fb-e1fe-4911-8417-6f6d55a1ce72', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('aab11f11-4f6e-4d16-9fae-f1b70e87bf7d', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '82b06e80-103e-4a38-b171-740d2b0e194b', 'admin', '2017-09-14 11:43:13');
INSERT INTO Sys_RoleAuthorize VALUES ('ac3c95a4-567e-4e52-90f6-40fa1046f930', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', 'c04bfd8f-7e2e-4312-9148-a2e14007fa46', 'admin', '2017-09-14 11:43:13');
INSERT INTO Sys_RoleAuthorize VALUES ('ad2014bd745b4010a98d7e40f355f64d', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', 'fbee5749-8694-495f-b140-b5b3399df7ee', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('b4e5effc529349b1923924d4e45c9154', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', 'e5346fa2-76ec-498f-8f54-3b443959335a', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('b7428619-8582-4489-b5e7-a065c9b4bd85', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '328b5383-79be-4b34-b57a-49fa3ebc7803', 'admin', '2017-09-14 11:43:13');
INSERT INTO Sys_RoleAuthorize VALUES ('bdfac344-f808-4a40-bf4a-d65ee8ddb901', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '85438f3b-0634-4b17-b778-aee3a5819669', 'admin', '2017-09-14 11:43:13');
INSERT INTO Sys_RoleAuthorize VALUES ('c1e0f9122eb84a53b8da13cbe07ae058', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '328b5383-79be-4b34-b57a-49fa3ebc7803', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('c43b755505a7414aacb369e557b72a0d', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '3de13971-a51f-40f7-be40-eb035b7f0fae', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('c7206703-c03f-43f4-bb1d-b610191659d0', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '09157352-1252-4964-8fee-479759a95db8', 'admin', '2017-09-14 11:43:13');
INSERT INTO Sys_RoleAuthorize VALUES ('c7409bf9-7a38-4a7d-8c29-d0b9c5583888', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '3de13971-a51f-40f7-be40-eb035b7f0fae', 'admin', '2017-09-14 11:43:12');
INSERT INTO Sys_RoleAuthorize VALUES ('d072a5b7-1c51-44d7-a538-ddf5acf6025e', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '026550fd-2578-42ae-a041-625cda12325f', 'admin', '2017-09-14 11:43:13');
INSERT INTO Sys_RoleAuthorize VALUES ('d870e70b-56ff-421e-8f47-90e26572f997', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '069f00f6-2a82-4bbe-90d6-418f37d5ef1f', 'admin', '2017-09-14 11:43:13');
INSERT INTO Sys_RoleAuthorize VALUES ('de2ecbb848c2467e9765ab1bdf6b3f6f', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '2d0b02db-09f7-4404-bbdd-c8a516f48288', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('de751358c525478ea52fa4ca6e3d4e11', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '7ae2e6aa-0433-4eaa-9357-1adec2507345', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('e7156c99-c2d5-423c-a397-8fa0480bb830', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '855f3590-b233-4224-aaff-47fb95c8353d', 'admin', '2017-09-14 11:43:12');
INSERT INTO Sys_RoleAuthorize VALUES ('eb643daa-c630-4b64-ae18-3f989b19b1e5', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '216d09a8-575f-43d1-85f6-acc025fa94b3', 'admin', '2017-09-14 11:43:12');
INSERT INTO Sys_RoleAuthorize VALUES ('ee8a2147e0fc4d71849f6b2a1c297aee', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '625cf550-4aad-4158-aff4-2a63d4f25819', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('ef22f6c09c404b93909bba350d5f8e97', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', '277c8647-ea81-42cf-8f7b-db353da95bbe', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('f61cf5c1-4926-4d22-8c93-20f99330f210', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '6d90439c-eb6b-4521-ab4d-5e481406a861', 'admin', '2017-09-14 11:43:12');
INSERT INTO Sys_RoleAuthorize VALUES ('fcfc70fb0efc4ecfb4212f4c7f28c19f', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', 'e00ba31fe82143299f4143e505345d83', 'admin', '2018-06-16 19:31:05');
INSERT INTO Sys_RoleAuthorize VALUES ('fe8110d3-1578-41cd-8ec5-40777d8c399b', 'a43d2b95-8ec0-44a2-b5ec-16c2e3390310', '2c24cdfc-8f26-4947-bcb2-0cb4d9111e80', 'admin', '2017-09-14 11:43:12');

CREATE TABLE Sys_User(
  Id varchar(50) NOT NULL COMMENT '主键',
  Account varchar(50) DEFAULT NULL COMMENT '账户',
  RealName varchar(50) DEFAULT NULL COMMENT '真实姓名',
  NickName varchar(50) DEFAULT NULL COMMENT '昵称',
  Avatar varchar(200) DEFAULT NULL COMMENT '头像',
  Gender char(1) DEFAULT NULL COMMENT '性别',
  Birthday datetime DEFAULT NULL COMMENT '生日',
  MobilePhone varchar(20) DEFAULT NULL COMMENT '手机',
  Email varchar(50) DEFAULT NULL COMMENT '邮箱',
  Signature varchar(500) DEFAULT NULL COMMENT '签名',
  Address varchar(500) DEFAULT NULL COMMENT '地址',
  CompanyId varchar(50) DEFAULT NULL COMMENT '领导ID',
  DepartmentId varchar(50) DEFAULT NULL COMMENT '组织ID',
  IsEnabled char(1) DEFAULT NULL COMMENT '是否启用',
  SortCode int(11) DEFAULT NULL COMMENT '排序码',
  DeleteMark char(1) DEFAULT NULL COMMENT '删除标记',
  CreateUser varchar(50) DEFAULT NULL COMMENT '创建人',
  CreateTime datetime DEFAULT NULL COMMENT '创建时间',
  ModifyUser varchar(50) DEFAULT NULL COMMENT '修改人',
  ModifyTime datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (Id)
);
INSERT INTO Sys_User VALUES ('d1ef3dcd-2c7d-4e8f-8f29-9f73625dd5df', 'admin', '管理员', '管理员', '/Content/framework/images/avatar.png', '1', '1991-07-22 00:00:00', '18800000000', '600000000@qq.com', '啦啦啦啦啦啦啦啦啦咯1', '江苏苏州', null, 'a93c66e2-b8dc-4d00-84ed-e6071b5f5318', '1', '1', '0', 'admin', '2017-03-22 10:58:43', 'admin', '2017-09-13 11:59:35');


CREATE TABLE `Sys_UserLogOn` (
  Id varchar(50) NOT NULL,
  UserId varchar(50) DEFAULT NULL,
  Password varchar(50) DEFAULT NULL,
  SecretKey varchar(50) DEFAULT NULL,
  PrevVisitTime datetime DEFAULT NULL,
  LastVisitTime datetime DEFAULT NULL,
  ChangePwdTime datetime DEFAULT NULL,
  LoginCount int(11) NOT NULL DEFAULT '0',
  AllowMultiUserOnline char(1) DEFAULT NULL,
  IsOnLine char(1) DEFAULT NULL,
  Question varchar(100) DEFAULT NULL,
  AnswerQuestion varchar(200) DEFAULT NULL,
  CheckIPAddress char(1) DEFAULT NULL,
  Language varchar(50) DEFAULT NULL,
  Theme varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
);

INSERT INTO Sys_UserLogOn VALUES ('6bde15b3-88a9-4522-817e-3d5877130a05', 'd1ef3dcd-2c7d-4e8f-8f29-9f73625dd5df', 'df5ce904bc0b9dcb5482c50e0b00675e', 'juhgtdjc', '2018-06-16 20:45:13', '2018-06-16 20:45:13', '2017-09-14 13:34:09', '1140', '1', '1', 'lovecoding?', 'no', '1', null, null);

CREATE TABLE Sys_UserRoleRelation (
  Id varchar(50) NOT NULL,
  UserId varchar(50) DEFAULT NULL,
  RoleId varchar(50) DEFAULT NULL,
  CreateUser varchar(50) DEFAULT NULL,
  CreateTime datetime DEFAULT NULL,
  PRIMARY KEY (Id)
);

INSERT INTO Sys_UserRoleRelation VALUES ('45e0a953-fd82-42f4-afe5-cbbbd2a263b0', 'd1ef3dcd-2c7d-4e8f-8f29-9f73625dd5df', 'a3a3857c-51fb-43a6-a7b5-3a612e887b3a', 'admin', '2017-01-20 09:37:08');
