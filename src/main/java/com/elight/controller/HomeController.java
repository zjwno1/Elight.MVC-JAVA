package com.elight.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.elight.entity.LayChildNavbar;
import com.elight.entity.LayNavbar;
import com.elight.entity.ModuleType;
import com.elight.entity.Operator;
import com.elight.filter.LoginChecked;
import com.elight.sys.entity.SysPermission;
import com.elight.sys.service.PermissionService;
import com.elight.utils.GsonUtils;
import com.elight.utils.OperatorProvider;
import com.elight.utils.PropertyPlaceholder;

@RestController
@RequestMapping(value = "/Home")
public class HomeController extends BaseController {

	@Autowired
	private PermissionService permissionService;
	/**
	 * 后台首页视图
	 * @param request
	 * @param session
	 * @return
	 */
	@LoginChecked()
	@RequestMapping(value = "/Index", method = { RequestMethod.GET })
	public ModelAndView index(HttpServletRequest request, HttpSession session) {
		Operator operator = OperatorProvider.getCurrent(session);
		if (operator != null) {
			ModelAndView view = new ModelAndView();
			view.addObject("SoftwareName", PropertyPlaceholder.getProperty("softwareName"));
			view.addObject("Account", operator.getAccount());
			view.addObject("Avatar", operator.getAvatar());
			view.setViewName("/System/Home/Index");
			return view;
		} else {
			return new ModelAndView("redirect:/Account/Login");
		}
	}

	/**
	 * 默认显示视图
	 * @return
	 */
	@RequestMapping(value = "/Default", method = { RequestMethod.GET })
	public ModelAndView defaultView() {
		return new ModelAndView("/System/Home/Default");
	}

	/**
	 * 获取左侧菜单
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/GetLeftMenu", method = { RequestMethod.POST },produces = "application/json; charset=utf-8")
    public String getLeftMenu(HttpServletRequest request)
    {
        String userId = OperatorProvider.getCurrent(request.getSession()).getUserId();
        String projectName = request.getContextPath();
        List<LayNavbar> listNavbar = new ArrayList<LayNavbar>();
        List<SysPermission> listModules = permissionService.getList(userId);
        for(SysPermission item : listModules) {
        	if(item.getType() == ModuleType.Menu && item.getLayer() == 0) {
        		LayNavbar navbarEntity = new LayNavbar();
        		List<LayChildNavbar> listChildNav =  getChildNavList(projectName,listModules,item.getId());
                navbarEntity.setIcon(item.getIcon());
                navbarEntity.setSpread(false);
                navbarEntity.setTitle(item.getName());
                navbarEntity.setChildren(listChildNav);
                listNavbar.add(navbarEntity);
        	}
        }
        return GsonUtils.objToJSON(listNavbar);
    }

	private List<LayChildNavbar> getChildNavList(String projectName,List<SysPermission> listModules, String parentId) {
		List<LayChildNavbar> list = new ArrayList<LayChildNavbar>();
		for (SysPermission item : listModules) {
			if (item.getType() == ModuleType.Menu && item.getLayer() == 1 && item.getParentId().equals(parentId)) {
				LayChildNavbar childNavbar = new LayChildNavbar();
				childNavbar.setHref(projectName+item.getUrl());
				childNavbar.setTitle(item.getName());
				childNavbar.setIcon(item.getIcon());
				list.add(childNavbar);
			}
		}
		return list;
	}

	 /**
	  * 获取登录用户权限
	  * @param session
	  * @return
	  */
	 @RequestMapping(value = "/GetPermission", method = { RequestMethod.POST },produces = "application/json; charset=utf-8")
	 public String getPermission(HttpSession session)
	 {
		 String userId = OperatorProvider.getCurrent(session).getUserId();
	     List<SysPermission> listModules = permissionService.getList(userId);
	     return GsonUtils.objToJSON(listModules);
	 }
}
