package com.elight.controller;

import com.elight.entity.AjaxResult;
import com.elight.entity.ResultType;
import com.elight.utils.GsonUtils;

public class BaseController {
	
	
	 protected String success()
     {
		 return GsonUtils.objToJSON(new AjaxResult(ResultType.Success,"恭喜您，操作成功。"));
     }
	 
	 protected String success(String message)
     {
		 return GsonUtils.objToJSON(new AjaxResult(ResultType.Success,message));
     }
	 protected String success(String message,String data)
     {
		 return GsonUtils.objToJSON(new AjaxResult(ResultType.Success,message,data));
     }
	 
	 protected String error()
     {
		 return GsonUtils.objToJSON(new AjaxResult(ResultType.Error,"对不起，操作失败。"));
     }
	 
     protected String error(String message)
     {
		 return GsonUtils.objToJSON(new AjaxResult(ResultType.Error,message));
     }
     
     protected String warning(String message)
     {
		 return GsonUtils.objToJSON(new AjaxResult(ResultType.Warning,message));
     }
     
     protected String info(String message)
     { 
    	 return GsonUtils.objToJSON(new AjaxResult(ResultType.Info,message));
     }
}
