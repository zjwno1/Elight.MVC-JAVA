package com.elight.controller;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.elight.entity.TreeSelect;
import com.elight.utils.ExcelUtils;
import com.elight.utils.PropertyPlaceholder;

@RestController
public class IndexController {

	@RequestMapping(value = "/index", method = { RequestMethod.GET })
	public ModelAndView index() {
		ModelAndView view = new ModelAndView();
		view.addObject("SoftwareName", PropertyPlaceholder.getProperty("softwareName"));
		view.setViewName("/System/Account/Login");
		return view;
	}

	@RequestMapping(value = "/ueditor", method = { RequestMethod.GET })
	public ModelAndView ueditor() {
		ModelAndView view = new ModelAndView();
		view.setViewName("/ueditor");
		return view;
	}

	
	 @RequestMapping(value = "/export")    
	 public void exportExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {    
	        List<TreeSelect> list = new ArrayList<TreeSelect>();    
	        list.add(new TreeSelect("123","456"));
	        list.add(new TreeSelect("456","789"));
	        list.add(new TreeSelect("789","101112"));
	        list.add(new TreeSelect("135","246"));   
	        Map<String,String> headMap =new HashMap<String,String>();
	        headMap.put("id", "ID");
	        headMap.put("text", "TEXT");
	        XSSFWorkbook wb = ExcelUtils.exportExcel(list, headMap, "名称", true);    
	        response.setContentType("application/vnd.ms-excel");    
	        response.setHeader("Content-disposition", "attachment;filename=student.xlsx");    
	        OutputStream ouputStream = response.getOutputStream();    
	        wb.write(ouputStream);    
	        ouputStream.flush();    
	        ouputStream.close();    
	   }    
}
