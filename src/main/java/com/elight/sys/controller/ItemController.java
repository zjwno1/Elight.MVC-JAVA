package com.elight.sys.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.elight.controller.BaseController;
import com.elight.entity.LayPadding;
import com.elight.entity.TreeSelect;
import com.elight.entity.ZTreeNode;
import com.elight.filter.AuthorizeChecked;
import com.elight.sys.entity.SysItem;
import com.elight.sys.service.SysItemService;
import com.elight.utils.GsonUtils;
import com.elight.utils.OperatorProvider;
import com.elight.utils.StringUtils;
import com.elight.utils.TreeSelectHelper;

@RestController
@RequestMapping(value = "/System/Item")
public class ItemController extends BaseController {
	@Autowired
	private SysItemService itemService;

	/**
	 * 字典列表画面
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Index", method = { RequestMethod.GET })
	public ModelAndView index() {
		return new ModelAndView("/System/Item/Index");
	}

	/**
	 * 查询字典（分页）
	 * @param pageIndex
	 * @param pageSize
	 * @param keyWord
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Index", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	public String index(int pageIndex, int pageSize, String keyWord) {
		int totalCount = itemService.getTotalCount(keyWord);
		List<SysItem> pageData = itemService.getList((pageIndex - 1) * pageSize, pageSize, keyWord);
		LayPadding<SysItem> result = new LayPadding<SysItem>();
		result.setResult(true);
		result.setMsg("success");
		result.setList(pageData);
		result.setCount(totalCount);
		return GsonUtils.objToJSON(result);
	}

	
	/**
	 * 字典新增或修改页面
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Form", method = { RequestMethod.GET })
	public ModelAndView form() {
		return new ModelAndView("/System/Item/Form");
	}

	/**
	 * 新增或者修改字典
	 * @param session
	 * @param model
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Form", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	public String form(HttpSession session, SysItem model) {
		if (StringUtils.isNullOrEmpty(model.getId())) {
			model.setCreateUser(OperatorProvider.getCurrent(session).getAccount());
			int row = itemService.insert(model);
			return row > 0 ? success() : error();
		} else {
			model.setModifyUser(OperatorProvider.getCurrent(session).getAccount());
			int row = itemService.update(model);
			return row > 0 ? success() : error();
		}
	}

	/**
	 * 获取字典信息
	 * @param primaryKey
	 * @return
	 */
	@RequestMapping(value = "/GetForm", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	public String getForm(String primaryKey) {
		SysItem entity = itemService.get(primaryKey);
		entity.setIsEnabled(entity.getIsEnabled().equals("1") ? "true" : "false");
		return GsonUtils.objToJSON(entity);
	}

	/**
	 * 删除字典信息
	 * @param primaryKey
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Delete", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	public String delete(String primaryKey) {
		int count = itemService.getChildCount(primaryKey);
		if (count == 0) {
			// 删除字典。
			int row = itemService.delete(primaryKey);
			// 删除字典选项。
			itemService.deleteItemDetail(primaryKey);
			return row > 0 ? success() : error();
		}
		return warning("操作失败，请先删除该项的" + count + "个子级字典。");
	}

	/**
	 * 字典详情画面
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Detail", method = { RequestMethod.GET })
	public ModelAndView detail() {
		return new ModelAndView("/System/Item/Detail");
	}

	/**
	 * 获取字典节点
	 * @return
	 */
	@RequestMapping(value = "/GetListTree", method = {RequestMethod.POST }, produces = "application/json; charset=utf-8")
	public String getListTree() {
		List<SysItem> listAllItems = itemService.getItemList();
		List<ZTreeNode> result = new ArrayList<ZTreeNode>();
		for (SysItem item : listAllItems) {
			ZTreeNode model = new ZTreeNode();
			model.setId(item.getId());
			model.setpId(item.getParentId());
			model.setName(item.getName());
			model.setOpen(true);
			result.add(model);
		}
		return GsonUtils.objToJSON(result);
	}

	/**
	 * 获得字典信息列表
	 * @return
	 */
	@RequestMapping(value = "/GetListSelectTree", method = { RequestMethod.POST },produces = "application/json; charset=utf-8")
    public String getListSelectTree()
    {
		List<SysItem> data = itemService.getItemList();
		List<TreeSelect> treeList = new ArrayList<TreeSelect>();
        for(SysItem item : data)
        {
            TreeSelect model = new TreeSelect();
            model.setId ( item.getId());
            model.setText (item.getName());
            model.setParentId ( item.getParentId());
            treeList.add(model);
        }
        return TreeSelectHelper.ToTreeSelectJson(treeList);
    }
}
