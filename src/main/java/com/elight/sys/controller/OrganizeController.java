package com.elight.sys.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.elight.controller.BaseController;
import com.elight.entity.LayPadding;
import com.elight.entity.TreeSelect;
import com.elight.filter.AuthorizeChecked;
import com.elight.sys.entity.SysOrganize;
import com.elight.sys.service.SysOrganizeService;
import com.elight.utils.GsonUtils;
import com.elight.utils.OperatorProvider;
import com.elight.utils.StringUtils;
import com.elight.utils.TreeSelectHelper;

@RestController
@RequestMapping(value = "/System/Organize")
public class OrganizeController extends BaseController{
	@Autowired
	private SysOrganizeService organizeService;
	
	/**
	 * 组织部门列表画面
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Index", method = { RequestMethod.GET })
    public ModelAndView index()
    {
		return new ModelAndView("/System/Organize/Index");
    }

    /**
     * 查询组织信息
     * @param pageIndex
     * @param pageSize
     * @param keyWord
     * @return
     */
	@AuthorizeChecked()
	@RequestMapping(value = "/Index", method = { RequestMethod.POST },produces = "application/json; charset=utf-8")
    public String index(int pageIndex, int pageSize, String keyWord)
    {
        int totalCount = organizeService.getTotalCount(keyWord);
        List<SysOrganize> pageData = organizeService.getList((pageIndex-1)*pageSize, pageSize, keyWord);
        LayPadding<SysOrganize> result = new LayPadding<SysOrganize>();
        result.setResult(true);
	    result.setMsg("success");
	    result.setList(pageData);
	    result.setCount(totalCount);
	    return GsonUtils.objToJSON(result);
    }

	/**
	 * 新增或者修改组织画面
	 * @return
	 */
	@AuthorizeChecked()
    @RequestMapping(value = "/Form", method = { RequestMethod.GET })
    public ModelAndView form()
    {
		return new ModelAndView("/System/Organize/Form");
    }

    /**
     * 插入修改组织信息
     * @param session
     * @param model
     * @return
     */
	@AuthorizeChecked()
	@RequestMapping(value = "/Form", method = { RequestMethod.POST },produces = "application/json; charset=utf-8")
    public String form(HttpSession session,SysOrganize model)
    {
		if (StringUtils.isNullOrEmpty(model.getId()))
        {
			model.setCreateUser(OperatorProvider.getCurrent(session).getAccount());
            int row = organizeService.insert(model);
            return row > 0 ? success() : error();
        }
        else
        {
            if(model.getParentId() == model.getId()){
                model.setParentId("0");
            }
        	model.setModifyUser(OperatorProvider.getCurrent(session).getAccount());
            int row = organizeService.update(model);
            return row > 0 ? success() : error();
        }
    }

    /**
     * 根据主键获取组织信息
     * @param primaryKey
     * @return
     */
	@RequestMapping(value = "/GetForm", method = { RequestMethod.POST },produces = "application/json; charset=utf-8")
    public String getForm(String primaryKey)
    {
		SysOrganize entity = organizeService.get(primaryKey);
        return GsonUtils.objToJSON(entity);
    }

    /**
     * 删除组织信息
     * @param primaryKey
     * @return
     */
	@AuthorizeChecked()
   	@RequestMapping(value = "/Delete", method = { RequestMethod.POST },produces = "application/json; charset=utf-8")
    public String delete(String primaryKey)
    {
        int count = organizeService.getChildCount(primaryKey);
        if (count == 0)
        {
            int row = organizeService.delete(primaryKey);
            return row > 0 ? success() : error();
        }
        return error("操作失败，请先删除该项的"+count+"个子级机构。");
    }

	/**
	 * 组织部门详情画面
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Detail", method = { RequestMethod.GET })
    public ModelAndView detail()
    {
		return new ModelAndView("/System/Organize/Detail");
    }

    /**
     * 获取组织信息
     * @return
     */
	@RequestMapping(value = "/GetListTreeSelect", method = { RequestMethod.POST },produces = "application/json; charset=utf-8")
    public String getListTreeSelect()
    {
        List<SysOrganize> data = organizeService.getOrganizeList();
        List<TreeSelect> treeList = new ArrayList<TreeSelect>();
        for (SysOrganize item : data)
        {
            TreeSelect model = new TreeSelect();
            model.setId(item.getId());
            model.setText(item.getFullName());
            model.setParentId (item.getParentId());
            treeList.add(model);
        }
        return TreeSelectHelper.ToTreeSelectJson(treeList);
    }
}
