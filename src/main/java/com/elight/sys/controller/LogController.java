package com.elight.sys.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.elight.utils.StringUtils;
import com.elight.controller.BaseController;
import com.elight.entity.LayPadding;
import com.elight.filter.AuthorizeChecked;
import com.elight.sys.entity.SysLog;
import com.elight.sys.service.SysLogService;
import com.elight.utils.DateUtils;
import com.elight.utils.GsonUtils;

@RestController
@RequestMapping(value = "/System/Log")
public class LogController extends BaseController {
	@Autowired
	private SysLogService logService;

	/**
	 * 日志列表画面
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Index", method = { RequestMethod.GET })
	public ModelAndView index() {
		return new ModelAndView("/System/Log/Index");
	}

	

	/**
	 * 查询日志信息
	 * @param pageIndex
	 * @param pageSize
	 * @param queryDate
	 * @param keyWord
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Index", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	public String index(int pageIndex, int pageSize, String queryDate, String keyWord) {
		Date limitDate=new Date();
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
		String today = format.format(limitDate);
		try {
			limitDate = format.parse(today);
		} catch (ParseException e) {
		}
		if (!StringUtils.isNullOrEmpty(queryDate)) {
			if (queryDate.equals("7")) {
				limitDate = DateUtils.plusDay(limitDate, -7);
			} else if (queryDate.equals("30")) {
				limitDate = DateUtils.plusMonth(limitDate, -1);
			} else if (queryDate.equals("90")) {
				limitDate = DateUtils.plusMonth(limitDate, -3);
			} else {
				try {
					limitDate = format.parse(today);
				} catch (ParseException e) {
				}
			}
		}
		int totalCount = logService.getTotalCount(limitDate, keyWord);
		List<SysLog> pageData = logService.getList((pageIndex-1)*pageSize, pageSize, limitDate, keyWord);
		LayPadding<SysLog> result = new LayPadding<SysLog>();
		result.setResult(true);
		result.setMsg("success");
		result.setList(pageData);
		result.setCount(totalCount);
		return GsonUtils.objToJSON(result);
	}

	/**
	 * 删除日志画面
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Delete", method = { RequestMethod.GET })
	public ModelAndView delete() {
		return new ModelAndView("/System/Log/Delete");
	}

	/**
	 * 删除日志
	 * @param keepType
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Delete", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	public String delete(String keepType) {
		Date keepDate = new Date();
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
		String today = format.format(keepDate);
		try {
			keepDate = format.parse(today);
		} catch (ParseException e) {
		}
		if (!StringUtils.isNullOrEmpty(keepType)) {
			if (keepType.equals("7")) {
				keepDate = DateUtils.plusDay(keepDate, -7);
			} else if (keepType.equals("30")) {
				keepDate = DateUtils.plusMonth(keepDate, -1);
			} else if (keepType.equals("30")) {
				keepDate = DateUtils.plusMonth(keepDate, -3);
			}else if (keepType.equals("0")) {
				keepDate =new Date();
			}
			else {
				try {
					keepDate = format.parse(today);
				} catch (ParseException e) {
				}
			}
			logService.delete(keepDate);
			return success();
		}
		return error();
	}
}
