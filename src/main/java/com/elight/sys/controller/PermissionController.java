package com.elight.sys.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.elight.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.elight.controller.BaseController;
import com.elight.entity.LayPadding;
import com.elight.entity.TreeSelect;
import com.elight.filter.AuthorizeChecked;
import com.elight.sys.entity.SysPermission;
import com.elight.sys.service.PermissionService;
import com.elight.utils.GsonUtils;
import com.elight.utils.OperatorProvider;
import com.elight.utils.TreeSelectHelper;

@RestController
@RequestMapping(value = "/System/Permission")
public class PermissionController  extends BaseController{
	@Autowired
	private PermissionService permissionService;
	
	/**
	 * 权限列表画面
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Index", method = { RequestMethod.GET })
	public ModelAndView index()
	{
		return new ModelAndView("/System/Permission/Index");
	}

	/**
	 * 权限列表查询（分页）
	 * @param pageIndex
	 * @param pageSize
	 * @param keyWord
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Index", method = { RequestMethod.POST },produces = "application/json; charset=utf-8")
	public String index(int pageIndex, int pageSize, String keyWord)
	{
		int totalCount = permissionService.getTotalCount(keyWord);
		List<SysPermission> pageData = permissionService.getList((pageIndex-1)*pageSize, pageSize, keyWord);
		LayPadding<SysPermission> result = new LayPadding<SysPermission>();
		result.setResult(true);
		result.setMsg("success");
		result.setList(pageData);
		result.setCount(totalCount);
		return GsonUtils.objToJSON(result);
	}

	/**
	 * 权限新增或者修改页面
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Form", method = { RequestMethod.GET })
	public ModelAndView form()
	{
		return new ModelAndView("/System/Permission/Form");
	}

	/**
	 * 插入修改权限信息
	 * @param session
	 * @param model
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Form", method = { RequestMethod.POST },produces = "application/json; charset=utf-8")
	public String form(HttpSession session,SysPermission model)
	{
		if(StringUtils.isNullOrEmpty(model.getId())) {
			 model.setCreateUser(OperatorProvider.getCurrent(session).getAccount());
             int row = permissionService.insert(model);
             return row >0 ? success() : error();
		}
		else {
			model.setModifyUser(OperatorProvider.getCurrent(session).getAccount());
            int row = permissionService.update(model);
            return row > 0 ? success() : error();
		}
	}

	/**
	 * 权限详情画面
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Detail", method = { RequestMethod.GET})
	public ModelAndView detail()
	{
		return new ModelAndView("/System/Permission/Detail");
	}


	/**
	 * 删除权限
	 * @param primaryKey
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Delete", method = { RequestMethod.POST },produces = "application/json; charset=utf-8")
	public String delete(String primaryKey)
	{
		int count = permissionService.getChildCount(primaryKey);
		if (count == 0)
		{
		     int row = permissionService.delete(primaryKey.split(","));
		     return row > 0 ? success() : error();
		}
		return error("操作失败，请先删除该项的"+count+"个子级权限。");
	}

	/**
	 * 根据主键得到权限信息
	 * @param primaryKey
	 * @return
	 */
	@RequestMapping(value = "/GetForm", method = { RequestMethod.POST },produces = "application/json; charset=utf-8")
	public String getForm(String primaryKey)
	{
		SysPermission entity = permissionService.get(primaryKey);
		entity.setIsEdit(entity.getIsEdit().equals("1")?"true":"false");
		entity.setIsEnable(entity.getIsEnable().equals("1") ? "true" : "false");
		entity.setIsPublic( entity.getIsPublic().equals("1") ? "true" : "false");
		return GsonUtils.objToJSON(entity);
	}

	
	/**
	 * 得到权限列表（下拉列表层级关系）
	 * @return
	 */
	@RequestMapping(value = "/GetParent", method = { RequestMethod.POST },produces = "application/json; charset=utf-8")
	public String getParent()
	{
		List<SysPermission> data = permissionService.getPermissionList();
		List<TreeSelect> treeList = new ArrayList<TreeSelect>();
		for (SysPermission item : data)
		{
		    TreeSelect model = new TreeSelect();
		    model.setId (item.getId());
		    model.setText(item.getName());
		    model.setParentId(item.getParentId());
		    treeList.add(model);
		}
		return TreeSelectHelper.ToTreeSelectJson(treeList);
	}

	/**
	 * 选择图标画面
	 * @return
	 */
	@RequestMapping(value = "/Icon", method = { RequestMethod.GET })
	public ModelAndView icon()
	{
	      return new ModelAndView("/System/Permission/Icon");
	}
}
