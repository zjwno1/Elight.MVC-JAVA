package com.elight.sys.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.elight.controller.BaseController;
import com.elight.entity.LayPadding;
import com.elight.filter.AuthorizeChecked;
import com.elight.sys.entity.SysItemsDetail;
import com.elight.sys.service.SysItemDetailService;
import com.elight.utils.GsonUtils;
import com.elight.utils.OperatorProvider;
import com.elight.utils.StringUtils;

@RestController
@RequestMapping(value = "/System/ItemsDetail")
public class ItemDetailController extends BaseController {
	@Autowired
	private SysItemDetailService itemDetailService;

	/**
	 * 字典明细列表画面
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Index", method = { RequestMethod.GET })
	public ModelAndView index() {
		return new ModelAndView("/System/ItemsDetail/Index");
	}

	/**
	 * 字典明细画面查询（分页）
	 * @param pageIndex
	 * @param pageSize
	 * @param itemId
	 * @param keyWord
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Index", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	public String index(int pageIndex, int pageSize, String itemId, String keyWord) {
		int totalCount = itemDetailService.getTotalCount(itemId,keyWord);
		List<SysItemsDetail> pageData = itemDetailService.getList((pageIndex - 1) * pageSize, pageSize,itemId, keyWord);
		LayPadding<SysItemsDetail> result = new LayPadding<SysItemsDetail>();
		result.setResult(true);
		result.setMsg("success");
		result.setList(pageData);
		result.setCount(totalCount);
		return GsonUtils.objToJSON(result);
	}

	/**
	 * 字典明细新增或者修改画面
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Form", method = { RequestMethod.GET })
	public ModelAndView form() {
		return new ModelAndView("/System/ItemsDetail/Form");
	}

	
	/**
	 * 字典明细新增或者修改
	 * @param session
	 * @param model
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Form", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	public String form(HttpSession session, SysItemsDetail model) {
		if (StringUtils.isNullOrEmpty(model.getId())) {
			model.setCreateUser(OperatorProvider.getCurrent(session).getAccount());
			int row = itemDetailService.insert(model);
			return row > 0 ? success() : error();
		} else {
			model.setModifyUser(OperatorProvider.getCurrent(session).getAccount());
			int row = itemDetailService.update(model);
			return row > 0 ? success() : error();
		}
	}

	/**
	 * 字典明细详情画面
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Detail", method = { RequestMethod.GET })
	public ModelAndView detail() {
		return new ModelAndView("/System/ItemsDetail/Detail");
	}

	/**
	 * 删除字典明细
	 * @param primaryKey
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Delete", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	public String delete(String primaryKey) {
		int row = itemDetailService.delete(primaryKey);
		return row > 0 ? success() : error();
	}

	/**
	 * 获取字典明细信息
	 * @param primaryKey
	 * @return
	 */
	@RequestMapping(value = "/GetForm", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	public String getForm(String primaryKey) {
		SysItemsDetail entity = itemDetailService.get(primaryKey);
		entity.setIsDefault(entity.getIsDefault().equals("1")? "true" : "false");
        entity.setIsEnabled(entity.getIsEnabled().equals("1") ? "true" : "false");
		return GsonUtils.objToJSON(entity);
	}
}