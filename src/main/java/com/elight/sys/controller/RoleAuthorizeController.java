package com.elight.sys.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.elight.controller.BaseController;
import com.elight.entity.ZTreeNode;
import com.elight.filter.AuthorizeChecked;
import com.elight.sys.service.RoleAuthorizeService;
import com.elight.utils.GsonUtils;

@RestController
@RequestMapping(value = "/System/RoleAuthorize")
public class RoleAuthorizeController extends BaseController{
	@Autowired
	private RoleAuthorizeService roleAuthorizeService;
	
	/**
	 * 角色授权页面
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Index", method = { RequestMethod.GET })
    public ModelAndView index()
    {
        return new ModelAndView("/System/RoleAuthorize/Index");
    }

    /**
     * 根据角色ID获得角色权限关系
     * @param roleId
     * @return
     */
	@RequestMapping(value = "/Index", method = { RequestMethod.POST },produces = "application/json; charset=utf-8")
    public String index(String roleId)
    {
		List<ZTreeNode> list = roleAuthorizeService.getZTreeNode(roleId);
        return GsonUtils.objToJSON(list);
    }
    
	/**
	 * 给某个角色授权
	 * @param session
	 * @param roleId
	 * @param perIds
	 * @return
	 */
	@RequestMapping(value = "/Form", method = { RequestMethod.POST },produces = "application/json; charset=utf-8")
    public String form(HttpSession session,String roleId, String perIds)
    {
		roleAuthorizeService.authorize(session, roleId, perIds.split(","));
        return success("授权成功");
    }
}
