package com.elight.sys.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.elight.controller.BaseController;
import com.elight.entity.LayPadding;
import com.elight.entity.TreeSelect;
import com.elight.filter.AuthorizeChecked;
import com.elight.sys.entity.SysRole;
import com.elight.sys.service.SysRoleService;
import com.elight.utils.GsonUtils;
import com.elight.utils.OperatorProvider;
import com.elight.utils.StringUtils;
import com.elight.utils.TreeSelectHelper;

@RestController
@RequestMapping(value = "/System/Role")
public class RoleController extends BaseController{
	@Autowired
	private SysRoleService roleService;
	
	/**
	 * 角色列表页
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Index", method = { RequestMethod.GET })
	public ModelAndView index(){
		return new ModelAndView("/System/Role/Index");
	}
	
	
	/**
	 * 角色列表查询（分页）
	 * @param pageIndex
	 * @param pageSize
	 * @param keyWord
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Index", method = { RequestMethod.POST },produces = "application/json; charset=utf-8")
	public String index(int pageIndex, int pageSize, String keyWord)
	{
		 int totalCount = roleService.getTotalCount(keyWord);
		 List<SysRole> pageData = roleService.getList((pageIndex-1)*pageSize, pageSize, keyWord);
		 LayPadding<SysRole> result = new LayPadding<SysRole>();
		 result.setResult(true);
	     result.setMsg("success");
	     result.setList(pageData);
	     result.setCount(totalCount);
	     return GsonUtils.objToJSON(result);
	}
	
	/**
	 * 角色新增修改页面
	 * @return
	 */
	@AuthorizeChecked()
    @RequestMapping(value = "/Form", method = { RequestMethod.GET })
	public ModelAndView form()
	{
		 return new ModelAndView("/System/Role/Form");
	}
	
	/**
	 * 角色新增或者修改
	 * @param session
	 * @param model
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Form", method = { RequestMethod.POST },produces = "application/json; charset=utf-8")
	public String form(HttpSession session,SysRole model)
	{
		if (StringUtils.isNullOrEmpty(model.getId()))
		{
		    model.setCreateUser(OperatorProvider.getCurrent(session).getAccount());
		    int row = roleService.insert(model);
		    return row >0 ? success() : error();
		}else{
		    model.setModifyUser(OperatorProvider.getCurrent(session).getAccount());
		    int row = roleService.update(model);
		    return row > 0 ? success() : error();
		}
	}

	/**
	 * 角色详情页面
	 * @return
	 */
	@AuthorizeChecked()
	@RequestMapping(value = "/Detail", method = { RequestMethod.GET })
	public ModelAndView detail()
	{
		 return new ModelAndView("/System/Role/Detail");
	}
	
	/**
	 * 根据主键得到角色信息
	 * @param primaryKey
	 * @return
	 */
	@RequestMapping(value = "/GetForm", method = { RequestMethod.POST },produces = "application/json; charset=utf-8")
	public String getForm(String primaryKey)
	{
		SysRole entity = roleService.get(primaryKey);
		entity.setIsEnabled(entity.getIsEnabled().equals("1") ? "true" : "false");
		entity.setAllowEdit( entity.getAllowEdit().equals("1") ? "true" : "false");
		return GsonUtils.objToJSON(entity);
	}
	/**
	 * 删除角色信息
	 * @param primaryKey
	 * @return
	 */
	@AuthorizeChecked()
   	@RequestMapping(value = "/Delete", method = { RequestMethod.POST },produces = "application/json; charset=utf-8")
	public String delete(String primaryKey)
	{
		int row = roleService.delete(primaryKey.split(","));
		return row > 0 ? success() : error();
	}

	/**
	 * 得到角色列表
	 * @return
	 */
	@RequestMapping(value = "/GetListTreeSelect", method = { RequestMethod.POST },produces = "application/json; charset=utf-8")
	public String getListTreeSelect()
	{
		List<SysRole> listRole = roleService.getRoleList();
		List<TreeSelect> listTree = new ArrayList<TreeSelect>();
		for(SysRole item : listRole)
		{
			TreeSelect model = new TreeSelect();
		    model.setId(item.getId());
		    model.setText(item.getName());
		    listTree.add(model);
		 }
		return GsonUtils.objToJSON(listTree);
	}
}
