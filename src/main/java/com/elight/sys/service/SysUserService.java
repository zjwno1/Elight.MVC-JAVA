package com.elight.sys.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.elight.sys.entity.SysUser;
import com.elight.sys.entity.SysUserLogOn;
import com.elight.sys.entity.SysUserRoleRelation;

public interface SysUserService {

	int getTotalCount(String keyWord);

	List<SysUser> getList(int pageIndex, int pageSize, String keyWord);

	int insert(SysUser model);

	void setRole(HttpSession session, String userId, String[] roleIdArray);

	int insertUserLogOn(SysUserLogOn model);

	int update(SysUser model);

	SysUser get(String primaryKey);

	List<SysUserRoleRelation> getUserRoleList(String userId);

	int delete(String[] userIdsArray);

	void deleteUserRoleRelation(String[] userIdsArray);

	void deleteLogOnUser(String[] userIdsArray);

	SysUser getByUserName(String userName);

}
