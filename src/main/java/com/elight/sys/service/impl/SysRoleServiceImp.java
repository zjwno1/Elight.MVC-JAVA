package com.elight.sys.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elight.sys.dao.SysRoleDao;
import com.elight.sys.entity.SysRole;
import com.elight.sys.service.SysRoleService;

@Service
public class SysRoleServiceImp implements SysRoleService {
	@Autowired
	private SysRoleDao roleDao;

	@Override
	public int getTotalCount(String keyWord) {
		return roleDao.getTotalCount(keyWord);
	}

	@Override
	public List<SysRole> getList(int pageIndex, int pageSize, String keyWord) {
		return roleDao.getList(pageIndex, pageSize, keyWord);
	}

	@Override
	public int insert(SysRole model) {
		model.setId(UUID.randomUUID().toString().replace("-", ""));
		model.setIsEnabled(model.getIsEnabled() == null ? "0" : "1");
		model.setAllowEdit(model.getAllowEdit() == null ? "0" : "1");
		model.setDeleteMark("0");
		model.setCreateTime(new Date());
		model.setModifyUser(model.getCreateUser());
		model.setModifyTime(model.getCreateTime());
		return roleDao.insert(model);
	}

	@Override
	public int update(SysRole model) {
		model.setIsEnabled(model.getIsEnabled() == null ? "0" : "1");
		model.setAllowEdit(model.getAllowEdit() == null ? "0" : "1");
		model.setModifyTime(new Date());
		return roleDao.update(model);
	}

	@Override
	public SysRole get(String primaryKey) {
		return roleDao.get(primaryKey);
	}

	@Override
	@Transactional
	public int delete(String[] primaryKeyArray) {
		for(String primaryKey :primaryKeyArray) {
			roleDao.delete(primaryKey);
		}
		return 1;
	}

	@Override
	public List<SysRole> getRoleList() {
		return roleDao.getRoleList();
	}
}
