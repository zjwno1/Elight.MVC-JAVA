package com.elight.sys.service.impl;

import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elight.sys.dao.SysLogDao;
import com.elight.sys.entity.SysLog;
import com.elight.sys.service.LogService;

@Service
public class LogServiceImp implements LogService {
	@Autowired
	private SysLogDao sysLogDao;


	@Override
	public int write(String logLevel, String operation, String message, String account, String realName, String ip, String browser) {
		SysLog entity = new SysLog();
		entity.setId(UUID.randomUUID().toString().replaceAll("-", ""));
		entity.setCreateTime(new Date());
		entity.setLogLevel(logLevel);
		entity.setOperation(operation);
		entity.setMessage(message);
		entity.setAccount(account);
		entity.setRealName(realName);
		entity.setIp(ip);
		entity.setIpAddress("");
		entity.setBrowser(browser);
		return sysLogDao.insert(entity);
	}

}
