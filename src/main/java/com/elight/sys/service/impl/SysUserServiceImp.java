package com.elight.sys.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elight.sys.dao.SysUserDao;
import com.elight.sys.dao.SysUserLogOnDao;
import com.elight.sys.dao.SysUserRoleRelationDao;
import com.elight.sys.entity.SysUser;
import com.elight.sys.entity.SysUserLogOn;
import com.elight.sys.entity.SysUserRoleRelation;
import com.elight.sys.service.SysUserService;
import com.elight.utils.Encrypt;
import com.elight.utils.MD5Util;
import com.elight.utils.OperatorProvider;

@Service
public class SysUserServiceImp implements SysUserService {
	@Autowired
	private SysUserDao userDao;
	@Autowired
	private SysUserLogOnDao userLogOnDao;
	@Autowired
	private SysUserRoleRelationDao userRoleRelationDao;

	@Override
	public int getTotalCount(String keyWord) {
		return userDao.getTotalCount(keyWord);
	}

	@Override
	public List<SysUser> getList(int pageIndex, int pageSize, String keyWord) {
		return userDao.getList(pageIndex, pageSize, keyWord);
	}

	@Override
	public int insert(SysUser model) {
		model.setId(UUID.randomUUID().toString().replace("-", ""));
		model.setDeleteMark("0");
		model.setCreateTime(new Date());
		model.setModifyUser(model.getCreateUser());
		model.setModifyTime(model.getCreateTime());
		model.setAvatar("/Content/framework/images/avatar.png");
		return userDao.insert(model);
	}

	@Override
	@Transactional
	public void setRole(HttpSession session, String userId, String[] roleIdArray) {
		List<SysUserRoleRelation> listOldRRs = userRoleRelationDao.getUserRoleRelations(userId);
		for (SysUserRoleRelation item : listOldRRs) {
			userRoleRelationDao.delete(item.getId());
		}
		String createUser = OperatorProvider.getCurrent(session).getAccount();
		for (String roleId : roleIdArray) {
			SysUserRoleRelation item = new SysUserRoleRelation();
			item.setId(UUID.randomUUID().toString().replace("-", ""));
			item.setUserId(userId);
			item.setRoleId(roleId);
			item.setCreateTime(new Date());
			item.setCreateUser(createUser);
			userRoleRelationDao.insert(item);
		}
	}

	@Override
	public int insertUserLogOn(SysUserLogOn model) {
		model.setId(UUID.randomUUID().toString().replace("-", ""));
		model.setSecretKey(model.getId().substring(0,8));
		model.setPassword(MD5Util.MD5(Encrypt.DESEncrypt(model.getPassword(),model.getSecretKey())).toLowerCase());
		model.setLoginCount(0);
		model.setIsOnLine("0");
		return userLogOnDao.insert(model);
	}

	@Override
	public int update(SysUser model) {
		model.setModifyTime(new Date());
		return userDao.update(model);
	}

	@Override
	public SysUser get(String primaryKey) {
		return userDao.get(primaryKey);
	}

	@Override
	public List<SysUserRoleRelation> getUserRoleList(String userId) {
		return userRoleRelationDao.getUserRoleRelations(userId);
	}

	@Override
	public int delete(String[] userIdsArray) {
		for (String primaryKey : userIdsArray) {
			userDao.delete(primaryKey);
		}
		return 1;
	}

	@Override
	@Transactional
	public void deleteUserRoleRelation(String[] userIdsArray) {
		for (String primaryKey : userIdsArray) {
			userRoleRelationDao.deleteByUserId(primaryKey);
		}
	}

	@Transactional
	public void deleteLogOnUser(String[] userIdsArray) {
		for (String primaryKey : userIdsArray) {
			userLogOnDao.delete(primaryKey);
		}
	}

	@Override
	public SysUser getByUserName(String userName) {
		return userDao.getByUserName(userName);
	}

}
