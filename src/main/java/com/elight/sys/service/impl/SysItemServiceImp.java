package com.elight.sys.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elight.sys.dao.SysItemDao;
import com.elight.sys.dao.SysItemDetailDao;
import com.elight.sys.entity.SysItem;
import com.elight.sys.service.SysItemService;

@Service
public class SysItemServiceImp implements SysItemService {
	@Autowired
	private SysItemDao itemDao;
	@Autowired
	private SysItemDetailDao itemDetailDao;

	@Override
	public int getTotalCount(String keyWord) {
		return itemDao.getTotalCount(keyWord);
	}

	@Override
	public List<SysItem> getList(int pageIndex, int pageSize, String keyWord) {
		return itemDao.getList(pageIndex, pageSize, keyWord);
	}

	@Override
	public int insert(SysItem model) {
		model.setId(UUID.randomUUID().toString().replace("-", ""));
		model.setLayer(get(model.getParentId()).getLayer() + 1);
		model.setIsEnabled(model.getIsEnabled() == null ? "0" : "1");
		model.setDeleteMark("0");
		model.setCreateTime(new Date());
		model.setModifyUser(model.getCreateUser());
		model.setModifyTime(model.getCreateTime());
		return itemDao.insert(model);
	}

	@Override
	public int update(SysItem model) {
		model.setLayer(get(model.getParentId()).getLayer() + 1);
		model.setIsEnabled(model.getIsEnabled() == null ? "0" : "1");
		model.setModifyTime(new Date());
		return itemDao.update(model);
	}

	@Override
	public SysItem get(String primaryKey) {
		return itemDao.get(primaryKey);
	}

	@Override
	public int getChildCount(String primaryKey) {
		return itemDao.getChildCount(primaryKey);
	}

	@Override
	public int delete(String primaryKey) {
		return itemDao.delete(primaryKey);
	}

	@Override
	public int deleteItemDetail(String primaryKey) {
		return itemDetailDao.delete(primaryKey);
	}

	@Override
	public List<SysItem> getItemList() {
		return itemDao.getItemList();
	}

}
