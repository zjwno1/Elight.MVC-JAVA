package com.elight.sys.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elight.sys.dao.SysPermissionDao;
import com.elight.sys.dao.SysRoleAuthorizeDao;
import com.elight.sys.dao.SysUserRoleRelationDao;
import com.elight.sys.entity.SysPermission;
import com.elight.sys.entity.SysRoleAuthorize;
import com.elight.sys.entity.SysUserRoleRelation;
import com.elight.sys.service.PermissionService;
import com.elight.utils.StringUtils;

@Service
public class PermissionServiceImp implements PermissionService {
	@Autowired
	private SysUserRoleRelationDao userRoleRelationDao;
	@Autowired
	private SysPermissionDao permissionDao;
	@Autowired
	private SysRoleAuthorizeDao roleAuthorizeDao;

	@Override
	public List<SysPermission> getList(String userId) {
		List<String> listRoleIds = new ArrayList<String>();
		// a.根据用户ID查询角色ID集合 （一对多关系）
		List<SysUserRoleRelation> userRoleRelationList = userRoleRelationDao.getUserRoleRelations(userId);
		for (SysUserRoleRelation item : userRoleRelationList) {
			if (!listRoleIds.contains(item.getRoleId()))
				listRoleIds.add(item.getRoleId());
		}
		// b.根据角色ID查询权限ID集合 （多对多关系）
		List<SysRoleAuthorize> roleAuthorizeList = roleAuthorizeDao.getRoleAuthorizeList();
		List<String> listModuleIds = new ArrayList<String>();
		for (SysRoleAuthorize item : roleAuthorizeList) {
			if (listRoleIds.contains(item.getRoleId())) {
				if (!listModuleIds.contains(item.getModuleId())) {
					listModuleIds.add(item.getModuleId());
				}
			}
		}
		// c.根据权限ID集合查询所有权限实体。
		List<SysPermission> permissionList = permissionDao.getPermissionList();
		List<SysPermission> retList = new ArrayList<SysPermission>();
		for (SysPermission permission : permissionList) {
			if (listModuleIds.contains(permission.getId()) && permission.getIsEnable().equals("1")) {
				retList.add(permission);
			}
		}
		return retList;
	}

	@Override
	public boolean actionValidate(String userId, String action) {
		List<SysPermission> authorizeModules = getList(userId);
		for (SysPermission item : authorizeModules) {
			if (!StringUtils.isNullOrEmpty(item.getUrl())) {
				String url = item.getUrl();
				if (url.contains("?")) {
					url = url.split("?")[0];
				}
				if (action.contains("?")) {
					action = action.split("?")[0];
				}
				if (action.toLowerCase().contains(url.toLowerCase())) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public int getTotalCount(String keyWord) {
		return permissionDao.getTotalCount(keyWord);
	}

	@Override
	public List<SysPermission> getList(int pageIndex, int pageSize, String keyWord) {
		return permissionDao.getList(pageIndex, pageSize, keyWord);
	}

	@Override
	public int insert(SysPermission model) {
		model.setId(UUID.randomUUID().toString().replace("-", ""));
		model.setLayer(permissionDao.get(model.getParentId()).getLayer() + 1);
		model.setIsEnable(model.getIsEnable() == null ? "0" : "1");
		model.setIsEdit(model.getIsEdit() == null ? "0" : "1");
		model.setIsPublic(model.getIsPublic() == null ? "0" : "1");
		model.setDeleteMark("0");
		model.setCreateTime(new Date());
		model.setModifyUser(model.getCreateUser());
		model.setModifyTime(model.getCreateTime());
		return permissionDao.insert(model);
	}

	@Override
	public int update(SysPermission model) {
		model.setLayer(permissionDao.get(model.getParentId()).getLayer() + 1);
		model.setIsEnable(model.getIsEnable() == null ? "0" : "1");
		model.setIsEdit(model.getIsEdit() == null ? "0" : "1");
		model.setIsPublic(model.getIsPublic() == null ? "0" : "1");
		model.setModifyTime(new Date());
		return permissionDao.update(model);
	}

	@Override
	public int getChildCount(String primaryKey) {
		return permissionDao.getChildCount(primaryKey);
	}

	@Override
	@Transactional
	public int delete(String[] idArray) {
		for (String id : idArray) {
			roleAuthorizeDao.delete(id);
		}
		for (String id : idArray) {
			permissionDao.delete(id);
		}
		return 1;
	}

	@Override
	public SysPermission get(String primaryKey) {
		return permissionDao.get(primaryKey);
	}

	@Override
	public List<SysPermission> getPermissionList() {
		return permissionDao.getPermissionList();
	}

}
