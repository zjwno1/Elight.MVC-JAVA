package com.elight.sys.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elight.sys.dao.SysUserDao;
import com.elight.sys.dao.SysUserLogOnDao;
import com.elight.sys.entity.SysUser;
import com.elight.sys.entity.SysUserLogOn;
import com.elight.sys.service.AccountService;

@Service
public class AccountServiceImp implements AccountService{
	@Autowired
	private SysUserDao sysUserDao;
	@Autowired
	private SysUserLogOnDao sysUserLogOnDao;
	
	@Override
	public SysUser getByUserName(String userName) {
		return sysUserDao.getByUserName(userName);
	}
	@Override
	public int updateLogin(SysUserLogOn model) {
		model.setIsOnLine("1");
        model.setLastVisitTime(new Date());
        model.setPrevVisitTime(model.getLastVisitTime());
        model.setLoginCount(model.getLoginCount()+1);
		return sysUserLogOnDao.updateLogin(model);
	}
	
	@Override
	public SysUserLogOn getByAccount(String userId) {
		return sysUserLogOnDao.getByAccount(userId);
	}
	@Override
	public SysUser getUser(String userId) {
		return sysUserDao.get(userId);
	}
	@Override
	public int modifyPwd(SysUserLogOn userLoginEntity) {
		userLoginEntity.setChangePwdTime(new Date());
		return sysUserLogOnDao.modifyPwd(userLoginEntity);
	}
	@Override
	public int updateBasicInfo(SysUser model) {
		model.setModifyTime(new Date());
		return sysUserDao.updateBasicInfo(model);
	}

}
