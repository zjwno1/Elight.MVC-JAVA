package com.elight.sys.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elight.sys.dao.SysItemDetailDao;
import com.elight.sys.entity.SysItemsDetail;
import com.elight.sys.service.SysItemDetailService;

@Service
public class SysItemDetailServiceImp implements SysItemDetailService {
	@Autowired
	private SysItemDetailDao itemDetailDao;

	@Override
	public int getTotalCount(String itemId, String keyWord) {
		return itemDetailDao.getTotalCount(itemId, keyWord);
	}

	@Override
	public List<SysItemsDetail> getList(int pageIndex, int pageSize, String itemId, String keyWord) {
		return itemDetailDao.getList(pageIndex, pageSize, itemId, keyWord);
	}

	@Override
	public int insert(SysItemsDetail model) {
		model.setId(UUID.randomUUID().toString().replace("-", ""));
		model.setIsEnabled(model.getIsEnabled() == null ? "0" : "1");
		model.setIsDefault(model.getIsDefault() == null ? "0" : "1");
		model.setDeleteMark("0");

		model.setCreateTime(new Date());
		model.setModifyUser(model.getCreateUser());
		model.setModifyTime(model.getCreateTime());
		return itemDetailDao.insert(model);
	}

	@Override
	public int update(SysItemsDetail model) {
		model.setIsEnabled(model.getIsEnabled() == null ? "0" : "1");
		model.setIsDefault(model.getIsDefault() == null ? "0" : "1");
		model.setModifyTime(new Date());
		return itemDetailDao.update(model);
	}

	@Override
	public int delete(String primaryKey) {
		return itemDetailDao.delete(primaryKey);
	}

	@Override
	public SysItemsDetail get(String primaryKey) {
		return itemDetailDao.get(primaryKey);
	}

	@Override
	public List<SysItemsDetail> getItemDetailList(String strItemCode) {
		return itemDetailDao.getItemDetailList(strItemCode);
	}
}
