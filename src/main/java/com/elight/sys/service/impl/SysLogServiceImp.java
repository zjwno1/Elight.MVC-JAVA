package com.elight.sys.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elight.sys.dao.SysLogDao;
import com.elight.sys.entity.SysLog;
import com.elight.sys.service.SysLogService;
@Service
public class SysLogServiceImp implements SysLogService{
	@Autowired
	private SysLogDao logDao;

	@Override
	public int getTotalCount(Date limitDate, String keyWord) {
		return logDao.getTotalCount(limitDate, keyWord);
	}

	@Override
	public List<SysLog> getList(int pageIndex, int pageSize, Date limitDate, String keyWord) {
		return logDao.getList(pageIndex, pageSize, limitDate, keyWord);
	}

	@Override
	public int delete(Date keepDate) {
		return logDao.delete(keepDate);
	}
}
