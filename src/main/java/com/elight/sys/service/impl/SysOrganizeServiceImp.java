package com.elight.sys.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elight.sys.dao.SysOrganizeDao;
import com.elight.sys.entity.SysOrganize;
import com.elight.sys.service.SysOrganizeService;

@Service
public class SysOrganizeServiceImp implements SysOrganizeService {
	@Autowired
	private SysOrganizeDao organizeDao;

	@Override
	public int getTotalCount(String keyWord) {
		return organizeDao.getTotalCount(keyWord);
	}

	@Override
	public List<SysOrganize> getList(int pageIndex, int pageSize, String keyWord) {
		return organizeDao.getList(pageIndex, pageSize, keyWord);
	}

	@Override
	public int insert(SysOrganize model) {
		model.setId(UUID.randomUUID().toString().replace("-", ""));
		model.setLayer(get(model.getParentId()).getLayer() + 1);
		model.setDeleteMark("0");
		model.setCreateTime(new Date());
		model.setModifyUser(model.getCreateUser());
		model.setModifyTime(model.getCreateTime());
		return organizeDao.insert(model);
	}

	@Override
	public SysOrganize get(String primaryKey) {
		return organizeDao.get(primaryKey);
	}

	@Override
	public int update(SysOrganize model) {
		model.setModifyTime(new Date());
		return organizeDao.update(model);
	}

	@Override
	public int getChildCount(String primaryKey) {
		return organizeDao.getChildCount(primaryKey);
	}

	@Override
	public int delete(String primaryKey) {
		return organizeDao.delete(primaryKey);
	}

	@Override
	public List<SysOrganize> getOrganizeList() {
		return organizeDao.getOrganizeList();
	}

}
