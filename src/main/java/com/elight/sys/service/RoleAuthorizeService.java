package com.elight.sys.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.elight.entity.ZTreeNode;


public interface RoleAuthorizeService {

	void authorize(HttpSession session, String roleId, String[] perIdArray);

	List<ZTreeNode> getZTreeNode(String roleId);

}
