package com.elight.sys.service;

import java.util.Date;
import java.util.List;

import com.elight.sys.entity.SysLog;

public interface SysLogService {

	int getTotalCount(Date limitDate, String keyWord);

	List<SysLog> getList(int pageIndex, int pageSize, Date limitDate, String keyWord);
	
	int delete(Date keepDate);

}
