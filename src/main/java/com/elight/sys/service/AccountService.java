package com.elight.sys.service;

import com.elight.sys.entity.SysUser;
import com.elight.sys.entity.SysUserLogOn;

public interface AccountService {

	SysUser getByUserName(String userName);

	int updateLogin(SysUserLogOn userLogOnEntity);

	SysUserLogOn getByAccount(String id);

	SysUser getUser(String userId);

	int modifyPwd(SysUserLogOn userLoginEntity);

	int updateBasicInfo(SysUser model);

}
