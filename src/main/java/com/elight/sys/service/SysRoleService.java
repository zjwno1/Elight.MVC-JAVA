package com.elight.sys.service;

import java.util.List;

import com.elight.sys.entity.SysRole;

public interface SysRoleService {

	int getTotalCount(String keyWord);

	List<SysRole> getList(int pageIndex, int pageSize, String keyWord);

	int insert(SysRole model);

	int update(SysRole model);

	SysRole get(String primaryKey);

	int delete(String[] primaryKeyArray);

	List<SysRole> getRoleList();

}
