package com.elight.sys.service;

import java.util.List;

import com.elight.sys.entity.SysPermission;

public interface PermissionService {

	List<SysPermission> getList(String userId);

	boolean actionValidate(String userId, String action);

	int getTotalCount(String keyWord);

	List<SysPermission> getList(int pageIndex, int pageSize, String keyWord);

	int insert(SysPermission model);

	int update(SysPermission model);

	int getChildCount(String primaryKey);

	int delete(String[] idArray);

	SysPermission get(String primaryKey);

	List<SysPermission> getPermissionList();

}
