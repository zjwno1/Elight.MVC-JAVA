package com.elight.sys.service;

public interface LogService {

	int write(String logLevel, String operation, String message, String account, String realName, String ip, String browser);
	
}
