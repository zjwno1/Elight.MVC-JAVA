package com.elight.sys.service;

import java.util.List;

import com.elight.sys.entity.SysItem;

public interface SysItemService {

	int getTotalCount(String keyWord);

	List<SysItem> getList(int pageIndex, int pageSize, String keyWord);

	int insert(SysItem model);

	int update(SysItem model);

	SysItem get(String primaryKey);

	int getChildCount(String primaryKey);

	int delete(String primaryKey);

	int deleteItemDetail(String primaryKey);

	List<SysItem> getItemList();

}
