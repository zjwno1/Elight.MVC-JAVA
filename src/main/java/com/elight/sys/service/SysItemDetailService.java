package com.elight.sys.service;

import java.util.List;

import com.elight.sys.entity.SysItemsDetail;

public interface SysItemDetailService {

	int getTotalCount(String itemId, String keyWord);

	List<SysItemsDetail> getList(int pageIndex, int pageSize, String itemId, String keyWord);

	int insert(SysItemsDetail model);

	int update(SysItemsDetail model);

	int delete(String primaryKey);

	SysItemsDetail get(String primaryKey);

	List<SysItemsDetail> getItemDetailList(String strItemCode);

}
