package com.elight.sys.service;

import java.util.List;

import com.elight.sys.entity.SysOrganize;

public interface SysOrganizeService {

	int getTotalCount(String keyWord);

	List<SysOrganize> getList(int pageIndex, int pageSize, String keyWord);

	int insert(SysOrganize model);

	int update(SysOrganize model);
	
	SysOrganize get(String primaryKey);

	int getChildCount(String primaryKey);

	int delete(String primaryKey);

	List<SysOrganize> getOrganizeList();

}
