package com.elight.sys.entity;

import java.util.Date;

public class SysRoleAuthorize {
	private String id;

	private String roleId;

	private String moduleId;

	private String createUser;

	private Date createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}


	@Override
	public String toString() {
		return "SysRoleAuthorize [Id=" + id + ", RoleId=" + roleId + ", ModuleId=" + moduleId + ", CreateUser="
				+ createUser + ", CreateTime=" + createTime + "]";
	}
	
	

}
