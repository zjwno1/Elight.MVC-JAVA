package com.elight.sys.entity;

import java.util.Date;

public class SysPermission{

	private String id;

	private String parentId;

	private int layer;

	private String enCode;

	private String name;

	private String jsEvent;

	private String icon;

	private String url;

	private String remark;

	private int type;

	private int sortCode;

	private String isPublic;

	private String isEnable;

	private String isEdit;

	private String deleteMark;

	private String createUser;

	private Date createTime;

	private String modifyUser;

	private Date modifyTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public int getLayer() {
		return layer;
	}

	public void setLayer(int layer) {
		this.layer = layer;
	}

	public String getEnCode() {
		return enCode;
	}

	public void setEnCode(String enCode) {
		this.enCode = enCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJsEvent() {
		return jsEvent;
	}

	public void setJsEvent(String jsEvent) {
		this.jsEvent = jsEvent;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getSortCode() {
		return sortCode;
	}

	public void setSortCode(int sortCode) {
		this.sortCode = sortCode;
	}

	public String getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(String isPublic) {
		this.isPublic = isPublic;
	}

	public String getIsEnable() {
		return isEnable;
	}

	public void setIsEnable(String isEnable) {
		this.isEnable = isEnable;
	}

	public String getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}

	public String getDeleteMark() {
		return deleteMark;
	}

	public void setDeleteMark(String deleteMark) {
		this.deleteMark = deleteMark;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}


	@Override
	public String toString() {
		return "SysPermission [id=" + id + ", parentId=" + parentId + ", layer=" + layer + ", enCode=" + enCode
				+ ", name=" + name + ", jsEvent=" + jsEvent + ", icon=" + icon + ", url=" + url + ", remark=" + remark
				+ ", type=" + type + ", sortCode=" + sortCode + ", isPublic=" + isPublic + ", isEnable=" + isEnable
				+ ", isEdit=" + isEdit + ", deleteMark=" + deleteMark + ", createUser=" + createUser + ", createTime="
				+ createTime + ", modifyUser=" + modifyUser + ", modifyTime=" + modifyTime + "]";
	}
	
}
