package com.elight.sys.entity;

import java.util.Date;

public class SysLog{
	private String id;
	private Date createTime;

	private String logLevel;

	private String operation;

	private String message;

	private String account;

	private String realName;

	private String ip;

	private String ipAddress;

	private String browser;

	private String stackTrace;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getLogLevel() {
		return logLevel;
	}

	public void setLogLevel(String logLevel) {
		this.logLevel = logLevel;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getStackTrace() {
		return stackTrace;
	}

	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}


	@Override
	public String toString() {
		return "SysLog [id=" + id + ", logLevel=" + logLevel + ", operation=" + operation + ", message=" + message
				+ ", account=" + account + ", realName=" + realName + ", ip=" + ip + ", ipAddress=" + ipAddress
				+ ", browser=" + browser + ", stackTrace=" + stackTrace + "]";
	}
	
	

}
