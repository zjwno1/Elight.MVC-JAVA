package com.elight.sys.entity;

import java.util.Date;

public class SysUserLogOn  {
	private String id;

	private String userId;

	private String password;

	private String secretKey;

	private Date prevVisitTime;

	private Date lastVisitTime;

	private Date changePwdTime;

	private int loginCount;

	private String allowMultiUserOnline;

	private String isOnLine;

	private String question;

	private String answerQuestion;

	private String checkIPAddress;

	private String language;

	private String theme;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public Date getPrevVisitTime() {
		return prevVisitTime;
	}

	public void setPrevVisitTime(Date prevVisitTime) {
		this.prevVisitTime = prevVisitTime;
	}

	public Date getLastVisitTime() {
		return lastVisitTime;
	}

	public void setLastVisitTime(Date lastVisitTime) {
		this.lastVisitTime = lastVisitTime;
	}

	public Date getChangePwdTime() {
		return changePwdTime;
	}

	public void setChangePwdTime(Date changePwdTime) {
		this.changePwdTime = changePwdTime;
	}

	public int getLoginCount() {
		return loginCount;
	}

	public void setLoginCount(int loginCount) {
		this.loginCount = loginCount;
	}

	public String getAllowMultiUserOnline() {
		return allowMultiUserOnline;
	}

	public void setAllowMultiUserOnline(String allowMultiUserOnline) {
		this.allowMultiUserOnline = allowMultiUserOnline;
	}

	public String getIsOnLine() {
		return isOnLine;
	}

	public void setIsOnLine(String isOnLine) {
		this.isOnLine = isOnLine;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswerQuestion() {
		return answerQuestion;
	}

	public void setAnswerQuestion(String answerQuestion) {
		this.answerQuestion = answerQuestion;
	}

	public String getCheckIPAddress() {
		return checkIPAddress;
	}

	public void setCheckIPAddress(String checkIPAddress) {
		this.checkIPAddress = checkIPAddress;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}


	@Override
	public String toString() {
		return "SysUserLogOn [id=" + id + ", userId=" + userId + ", password=" + password + ", secretKey=" + secretKey
				+ ", loginCount=" + loginCount + ", allowMultiUserOnline=" + allowMultiUserOnline + ", isOnLine="
				+ isOnLine + ", question=" + question + ", answerQuestion=" + answerQuestion + ", checkIPAddress="
				+ checkIPAddress + ", language=" + language + ", theme=" + theme + "]";
	}
	
		
}
