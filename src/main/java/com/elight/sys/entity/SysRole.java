package com.elight.sys.entity;

import java.util.Date;

public class SysRole{

	private String id;

	private String organizeId;

	private String enCode ;

	private int type ;

	private String name;

	private String allowEdit ;

	private String deleteMark;

	private String isEnabled ;

	private String remark;

	private int sortCode;

	private String createUser;

	private Date createTime;

	private String modifyUser ;

	private Date modifyTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrganizeId() {
		return organizeId;
	}

	public void setOrganizeId(String organizeId) {
		this.organizeId = organizeId;
	}

	public String getEnCode() {
		return enCode;
	}

	public void setEnCode(String enCode) {
		this.enCode = enCode;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAllowEdit() {
		return allowEdit;
	}

	public void setAllowEdit(String allowEdit) {
		this.allowEdit = allowEdit;
	}

	public String getDeleteMark() {
		return deleteMark;
	}

	public void setDeleteMark(String deleteMark) {
		this.deleteMark = deleteMark;
	}

	public String getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(String isEnabled) {
		this.isEnabled = isEnabled;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public int getSortCode() {
		return sortCode;
	}

	public void setSortCode(int sortCode) {
		this.sortCode = sortCode;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}


	@Override
	public String toString() {
		return "SysRole [Id=" + id + ", OrganizeId=" + organizeId + ", EnCode=" + enCode + ", Type=" + type + ", Name="
				+ name + ", AllowEdit=" + allowEdit + ", DeleteMark=" + deleteMark + ", IsEnabled=" + isEnabled
				+ ", Remark=" + remark + ", SortCode=" + sortCode + ", CreateUser=" + createUser + ", CreateTime="
				+ createTime + ", ModifyUser=" + modifyUser + ", ModifyTime=" + modifyTime + "]";
	}
	
//    /// <summary>
//    /// 部门名称
//    /// </summary>
//    [SugarColumn(IsIgnore = true)]
	private String deptName;

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	
//
//    /// <summary>
//    /// 组织名称
//    /// </summary>
//    [SugarColumn(IsIgnore = true)]
//    public Sys_Organize OrganizeSingle
//    {
//        get
//        {
//            try
//            {
//                return base.CreateMapping<Sys_Organize>().Single(it => it.Id == this.OrganizeId);
//            }
//            catch
//            {
//                return new Sys_Organize();
//            }
//
//        }
//    }
	
}
