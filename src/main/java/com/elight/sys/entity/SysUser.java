package com.elight.sys.entity;

import java.util.Date;

public class SysUser {
	private String id;

	private String account;

	private String realName;

	private String nickName;

	private String avatar;

	private String gender;

	private Date birthday;

	private String mobilePhone;

	private String email;

	private String signature;

	private String address;

	private String companyId;

	private String isEnabled;

	private int sortCode;

	private String departmentId;

	private String deleteMark;

	private String createUser;

	private Date createTime;

	private String modifyUser;

	private Date modifyTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(String isEnabled) {
		this.isEnabled = isEnabled;
	}

	public int getSortCode() {
		return sortCode;
	}

	public void setSortCode(int sortCode) {
		this.sortCode = sortCode;
	}

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public String getDeleteMark() {
		return deleteMark;
	}

	public void setDeleteMark(String deleteMark) {
		this.deleteMark = deleteMark;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	private String strBirthDay;

	public String getStrBirthDay() {
		return strBirthDay;
	}

	public void setStrBirthDay(String strBirthDay) {
		this.strBirthDay = strBirthDay;
	}
	

	@Override
	public String toString() {
		return "SysUser [id=" + id + ", account=" + account + ", realName=" + realName + ", nickName=" + nickName
				+ ", avatar=" + avatar + ", gender=" + gender + ", birthday=" + birthday + ", mobilePhone="
				+ mobilePhone + ", email=" + email + ", signature=" + signature + ", address=" + address
				+ ", companyId=" + companyId + ", isEnabled=" + isEnabled + ", sortCode=" + sortCode + ", departmentId="
				+ departmentId + ", deleteMark=" + deleteMark + ", createUser=" + createUser + ", createTime="
				+ createTime + ", modifyUser=" + modifyUser + ", modifyTime=" + modifyTime + "]";
	}

	

	private String deptName;

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
}
