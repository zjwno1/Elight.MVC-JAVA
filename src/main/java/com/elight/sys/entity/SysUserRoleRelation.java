package com.elight.sys.entity;

import java.util.Date;

public class SysUserRoleRelation{
    public String id ;

    public String userId ;

    public String roleId;

    public String createUser;

    public Date createTime ;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}


	@Override
	public String toString() {
		return "SysUserRoleRelation [Id=" + id + ", UserId=" + userId + ", RoleId=" + roleId + ", CreateUser="
				+ createUser + ", CreateTime=" + createTime + "]";
	}
	
}
