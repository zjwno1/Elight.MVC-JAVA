package com.elight.sys.entity;
import java.util.Date;


public class SysItem{

    private String id ;

    private String enCode;

  
    private String parentId;

 
    private String name;

   
    private int layer;

    private int sortCode;

    private String isTree ;


    private String  deleteMark;

 
    private String  isEnabled;


    private String remark;


    private String  createUser ;

 
    private Date createTime ;

    private String ModifyUser ;

    private Date modifyTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEnCode() {
		return enCode;
	}

	public void setEnCode(String enCode) {
		this.enCode = enCode;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLayer() {
		return layer;
	}

	public void setLayer(int layer) {
		this.layer = layer;
	}

	public int getSortCode() {
		return sortCode;
	}

	public void setSortCode(int sortCode) {
		this.sortCode = sortCode;
	}

	public String getIsTree() {
		return isTree;
	}

	public void setIsTree(String isTree) {
		this.isTree = isTree;
	}

	public String getDeleteMark() {
		return deleteMark;
	}

	public void setDeleteMark(String deleteMark) {
		this.deleteMark = deleteMark;
	}

	public String getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(String isEnabled) {
		this.isEnabled = isEnabled;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getModifyUser() {
		return ModifyUser;
	}

	public void setModifyUser(String modifyUser) {
		ModifyUser = modifyUser;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}


	@Override
	public String toString() {
		return "SysItem [id=" + id + ", enCode=" + enCode + ", parentId=" + parentId + ", name=" + name + ", layer="
				+ layer + ", sortCode=" + sortCode + ", isTree=" + isTree + ", deleteMark=" + deleteMark
				+ ", isEnabled=" + isEnabled + ", remark=" + remark + ", createUser=" + createUser + ", createTime="
				+ createTime + ", ModifyUser=" + ModifyUser + ", modifyTime=" + modifyTime + "]";
	}
    
    
}
