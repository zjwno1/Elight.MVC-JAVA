package com.elight.sys.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.elight.sys.entity.SysLog;
@Repository
public interface SysLogDao {
	int getTotalCount(@Param("limitDate") Date limitDate, @Param("keyWord") String keyWord);

	List<SysLog> getList(@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("limitDate") Date limitDate, @Param("keyWord") String keyWord);

	int delete(@Param("keepDate") Date keepDate);

	int insert(SysLog entity);
}
