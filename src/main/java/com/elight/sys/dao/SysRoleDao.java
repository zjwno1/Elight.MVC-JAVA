package com.elight.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.elight.sys.entity.SysRole;

@Repository
public interface SysRoleDao {
	List<SysRole> getRoleList();
	
	SysRole get(@Param("primaryKey") String primaryKey);
	
	int getTotalCount(@Param("keyWord") String keyWord);
	
    List<SysRole> getList(@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("keyWord") String keyWord);

    int delete(@Param("primaryKey") String primaryKey);
    
    int insert(SysRole model);

    int update(SysRole model);

   
}
