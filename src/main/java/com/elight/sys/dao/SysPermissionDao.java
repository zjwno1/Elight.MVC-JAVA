package com.elight.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.elight.sys.entity.SysPermission;
@Repository
public interface SysPermissionDao {
	int getTotalCount(@Param("keyword") String keyword);

	List<SysPermission> getList(@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("keyword") String keyword);

	int delete(@Param("primaryKey") String primaryKey);

	int getChildCount(@Param("parentId") String parentId);

	List<SysPermission> getPermissionList();

	public SysPermission get(@Param("primaryKey") String primaryKey);

	int insert(SysPermission model);

	int update(SysPermission model);
}
