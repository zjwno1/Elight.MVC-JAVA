package com.elight.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.elight.sys.entity.SysUser;

@Repository
public interface SysUserDao {
	SysUser getByUserName(@Param(value = "account") String account);

	int updateBasicInfo(SysUser model);

	SysUser get(@Param("primaryKey") String primaryKey);

	int getTotalCount(@Param("keyWord") String keyWord);

	List<SysUser> getList(@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("keyWord") String keyWord);

	int delete(@Param("primaryKey") String primaryKey);

	int insert(SysUser model);

	int update(SysUser model);
}
