package com.elight.sys.dao;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.elight.sys.entity.SysUserLogOn;

@Repository
public interface SysUserLogOnDao {
    SysUserLogOn getByAccount(@Param("userId") String userId);

    int updateLogin(SysUserLogOn model);

    int modifyPwd(SysUserLogOn userLoginEntity);

    int delete(@Param("userId") String userId);

    int insert(SysUserLogOn model);

    int updateInfo(SysUserLogOn model);
}
