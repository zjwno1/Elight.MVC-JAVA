package com.elight.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.elight.sys.entity.SysOrganize;
@Repository
public interface SysOrganizeDao {

	List<SysOrganize> getOrganizeList();

	int getTotalCount(@Param("keyWord") String keyWord);

	List<SysOrganize> getList(@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("keyWord") String keyWord);

	int getChildCount(@Param("parentId") String parentId);
	
	SysOrganize get(@Param("primaryKey") String primaryKey);
	
	int delete(@Param("primaryKey") String primaryKey);
	
	int insert(SysOrganize model);
	

	int update(SysOrganize model);
}
