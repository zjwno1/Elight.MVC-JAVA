package com.elight.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.elight.sys.entity.SysUserRoleRelation;

@Repository
public interface SysUserRoleRelationDao {

	List<SysUserRoleRelation> getUserRoleRelations(@Param("userId") String userId);

	int delete(@Param("id") String id);

	int insert(SysUserRoleRelation item);

	void deleteByUserId(@Param("userId") String userId);

}
