package com.elight.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.elight.sys.entity.SysItem;
@Repository
public interface SysItemDao {
    List<SysItem> getItemList();

    int getTotalCount(@Param("keyWord") String keyWord);
    
    List<SysItem> getList(@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("keyWord") String keyWord);
   
    int getChildCount(@Param("parentId") String parentId);
   
    SysItem get(@Param("primaryKey") String primaryKey);
 
    int insert(SysItem model);
    
    int delete(@Param("primaryKey") String primaryKey);

    int update(SysItem model);
}
