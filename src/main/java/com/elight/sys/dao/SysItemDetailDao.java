package com.elight.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.elight.sys.entity.SysItemsDetail;
@Repository
public interface SysItemDetailDao {

    List<SysItemsDetail> getItemDetailList(@Param("strItemCode") String strItemCode);

    int getTotalCount(@Param("itemId") String itemId, @Param("keyWord") String keyWord);
    
    List<SysItemsDetail> getList(@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("itemId") String itemId, @Param("keyWord") String keyWord);

    SysItemsDetail get(@Param("primaryKey") String primaryKey);
    
    int insert(SysItemsDetail model);
    
    int delete(@Param("itemId") String itemId);

    int update(SysItemsDetail model);
}
