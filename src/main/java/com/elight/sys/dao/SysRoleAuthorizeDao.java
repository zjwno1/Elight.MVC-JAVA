package com.elight.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.elight.sys.entity.SysRoleAuthorize;

@Repository
public interface SysRoleAuthorizeDao {
	List<SysRoleAuthorize> getRoleAuthorizeList();

	List<SysRoleAuthorize> getList(@Param("roleId") String roleId);

	int delete(@Param("primaryKey") String primaryKey);

	int insert(SysRoleAuthorize entity);
}
