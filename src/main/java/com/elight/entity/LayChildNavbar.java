package com.elight.entity;

public class LayChildNavbar {
	/// <summary>
	/// 标题
	/// </summary>
	private String title;
	/// <summary>
	/// 图标
	/// </summary>
	private String icon;
	/// <summary>
	/// 链接
	/// </summary>
	private String href;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	@Override
	public String toString() {
		return "LayChildNavbar [title=" + title + ", icon=" + icon + ", href=" + href + "]";
	}
}
