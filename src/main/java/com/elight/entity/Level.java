package com.elight.entity;

public class Level {
	public final static String Trace = "普通输出";
	public final static String Debug = "一般调试";
	public final static String Info = "普通消息";
	public final static String Warn = "警告信息";
	public final static String Error = "一般错误";
	public final static String Fatal = "致命错误";
}
