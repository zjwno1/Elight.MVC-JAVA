package com.elight.entity;

public class AjaxResult {
	public AjaxResult(int state, String message) {
		this.state = state;
		this.message = message;
		this.data = null;
	}

	public AjaxResult(int state, String message, Object data) {
		this.state = state;
		this.message = message;
		this.data = data;
	}

	/// <summary>
	/// 结果类型。
	/// </summary>
	public int state;
	/// <summary>
	/// 消息内容。
	/// </summary>
	public String message;
	/// <summary>
	/// 返回数据。
	/// </summary>
	public Object data;
	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	
}
