package com.elight.entity;

public class ZTreeNode {
	/// <summary>
    /// 节点ID。
    /// </summary>
    private String id;
    /// <summary>
    /// 父节点ID。
    /// </summary>
    private String pId;
    /// <summary>
    /// 节点名称。
    /// </summary>
    private String name;
    /// <summary>
    /// 是否展开。
    /// </summary>
    private boolean open;
    /// <summary>
    /// 是否选中。
    /// </summary>
    private boolean checked;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getpId() {
		return pId;
	}
	public void setpId(String pId) {
		this.pId = pId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isOpen() {
		return open;
	}
	public void setOpen(boolean open) {
		this.open = open;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
    
    
}
