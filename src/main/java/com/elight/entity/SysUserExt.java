package com.elight.entity;

import java.util.List;

import com.elight.sys.entity.SysUser;

public class SysUserExt extends SysUser{
	private List<String> roleId;

	public void setRoleId(List<String> roleId) {
		this.roleId = roleId;
	}

	public List<String> getRoleId() {
		return roleId;
	}
}
