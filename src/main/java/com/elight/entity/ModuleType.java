package com.elight.entity;

public class ModuleType {
	/// <summary>
	/// 菜单。
	/// </summary>
	public final static int Menu = 0;
	/// <summary>
	/// 按钮。
	/// </summary>
	public final static int Button = 1;
	/// <summary>
	/// 请求。
	/// </summary>
	public final static int Ajax = 2;
}
