package com.elight.entity;

public class ResultType {
	/// <summary>
	/// 警告。
	/// </summary>
	public final static int Warning = 0;

	/// <summary>
	/// 成功。
	/// </summary>
	public final static int Success = 1;

	/// <summary>
	/// 异常。
	/// </summary>
	public final static int Error = 2;

	/// <summary>
	/// 消息。
	/// </summary>
	public final static int Info = 6;
}