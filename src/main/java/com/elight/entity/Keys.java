package com.elight.entity;

public class Keys {
	/// <summary>
    /// 标识验证码。
    /// </summary>
    public static String SESSION_KEY_VCODE = "verify_code";
    /// <summary>
    /// 标识当前登陆用户。
    /// </summary>
    public static String SESSION_KEY_USER = "user_info";
    /// <summary>
    /// 标识用户权限集合。
    /// </summary>
    public static String SESSION_KEY_USER_PER = "user_perssion";
}
