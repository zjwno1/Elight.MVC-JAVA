package com.elight.entity;

import java.util.List;

public class LayPadding<T>{
	public int code;

    /// <summary>
    /// 获取结果。
    /// </summary>
    public boolean result;

    /// <summary>
    /// 备注信息。
    /// </summary>
    public String msg;

    /// <summary>
    /// 数据列表。
    /// </summary>
    public List<T> list;

    /// <summary>
    /// 记录条数。
    /// </summary>
    public long count;
    private String backgroundImage;

	public String getBackgroundImage() {
		return backgroundImage;
	}

	public void setBackgroundImage(String backgroundImage) {
		this.backgroundImage = backgroundImage;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}
    
    
}
