package com.elight.entity;

import java.util.List;

public class LayNavbar {
	/// <summary>
	/// 标题
	/// </summary>
	private String title;
	/// <summary>
	/// 图标
	/// </summary>
	private String icon;
	/// <summary>
	/// 是否展开
	/// </summary>
	private boolean spread;
	/// <summary>
	/// 子级菜单集合
	/// </summary>
	private List<LayChildNavbar> children;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public boolean isSpread() {
		return spread;
	}
	public void setSpread(boolean spread) {
		this.spread = spread;
	}
	public List<LayChildNavbar> getChildren() {
		return children;
	}
	public void setChildren(List<LayChildNavbar> children) {
		this.children = children;
	}
	@Override
	public String toString() {
		return "LayNavbar [title=" + title + ", icon=" + icon + ", spread=" + spread + ", children=" + children + "]";
	}
	
	
}
