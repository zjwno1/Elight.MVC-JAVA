package com.elight.utils;

import javax.servlet.http.HttpSession;

import com.elight.entity.Operator;

public class OperatorProvider {

	public static Operator getCurrent(HttpSession session) {
		Object obj = session.getAttribute("LoginUser");
		if(obj==null)
			return null;
		String json = Encrypt.DESDecrypt((String)obj);
		Operator operator = GsonUtils.jsonToBean(json, Operator.class);
	    return operator;
	}

	public static void remove(HttpSession session) {
		session.removeAttribute("LoginUser");
	}

}
