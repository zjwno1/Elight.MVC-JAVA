package com.elight.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeUtils {
	/**
	 * @return yyyy-MM-dd HH:mm:ss
	 */
	public static String getNowTimeNormal(){
		return getNowTime("yyyy-MM-dd HH:mm:ss");
	}
	
	/**
	 * @return  yyyy-MM-dd HH:mm:ss.SSS
	 */
	public static String getNowTimeNormalWithMill(){
		return getNowTime("yyyy-MM-dd HH:mm:ss.SSS");
	}
	
	/**
	 * @param format
	 * @return StringDate
	 */
	public static String getNowTime(String format) {
		SimpleDateFormat format1 = new SimpleDateFormat(format);
		Date date = new Date();
		return format1.format(date);
	}
	
	/**
	 * @param String date
	 * @param String format
	 * @return Date
	 */
	public static Date parse(String date,String format){
		try {
			SimpleDateFormat dtformat=new SimpleDateFormat(format);
			return dtformat.parse(date);
		} catch (Exception e) {
		}
		return new Date();
	}

	/**
	 * @param date yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static Date parseNormal(String date){
		return parse(date,"yyyy-MM-dd HH:mm:ss");
	}
	
	/**
	 * @param date yyyy-MM-dd
	 * @return
	 */
	public static Date parseNormalDate(String date){
		return parse(date,"yyyy-MM-dd");
	}
	
	/**
	 * @param date yyyy-MM-dd HH:mm:ss.SSS
	 * @return
	 */
	public static Date parseNormalWithMill(String date){
		return parse(date,"yyyy-MM-dd HH:mm:ss.SSS");
	}

	/**
	 * 格式化日期
	 * @param date
	 * @param format
	 * @return
	 */
	public static String formatDate(Date date,String format) {
		SimpleDateFormat dateFormat= new SimpleDateFormat(format);
		return dateFormat.format(date);
	}
	
	
	public static Date addYear(Date date,int year){
		Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(date);
        rightNow.add(Calendar.YEAR,year);//日期加year年
        return rightNow.getTime();
	}
	
	public static Date addMonth(Date date,int month){
		Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(date);
        rightNow.add(Calendar.MONTH,month);//日期加month个月
        return rightNow.getTime();
	}
	
	public static Date addDay(Date date,int day){
		Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(date);
        rightNow.add(Calendar.DAY_OF_YEAR,day);//日期加10天
        return rightNow.getTime();
	}
	
	public static Date addHour(Date date,int hour){
		Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(date);
        rightNow.add(Calendar.HOUR_OF_DAY,hour);//日期加10天
        return rightNow.getTime();
	}
	
	public static Date addMinute(Date date,int minute){
		Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(date);
        rightNow.add(Calendar.MINUTE,minute);//日期加10天
        return rightNow.getTime();
	}
	
	public static Date addSecond(Date date,int second){
		Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(date);
        rightNow.add(Calendar.SECOND,second);
        return rightNow.getTime();
	}
	
	public static Date addMilliSecond(Date date,int milliSecond){
		Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(date);
        rightNow.add(Calendar.MILLISECOND,milliSecond);
        return rightNow.getTime();
	}
	
	public static long getDifference(Date startDate,Date endDate){
		return startDate.getTime()-endDate.getTime();
	}
}
