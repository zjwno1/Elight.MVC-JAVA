package com.elight.utils;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Hashtable;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
//import com.swetake.util.Qrcode;

/**
 * 生成二维码工具类
 * @author zhoujunwen
 *
 */
public class QRCodeUtils {

//	public static void main(String[] args) throws IOException {
//		BufferedImage image=createQrcode("我爱你",120);
//		ImageIO.write(image, "png", new File("d:/123.png")); //保存修改后的图像,全部保存为JPG格式
//	}
	
	/**
	 * 根据内容得到二维码  
	 * @param content
	 * 	   内容不超过120字
	 * @param width
	 * 	       宽度不超过4500
	 * @return
	 */
	public static BufferedImage createQrcode(String content,int width) {
		BufferedImage originalImage=createQrcode(content);
		BufferedImage newImage = new BufferedImage(width,width,originalImage.getType());
	    Graphics g = newImage.getGraphics();
	    g.drawImage(originalImage, 0,0,width,width,null);
	    g.dispose();
	    return newImage;
	}

	
	/**
	 * 根据内容得到二维码 4500*4500px
	 * @param content
	 * 			内容不超过120字
	 * @return
	 */
	public static BufferedImage createQrcode(String content) {
//		Qrcode qrc = new Qrcode();
//		// 设置参数
//		qrc.setQrcodeEncodeMode('B');
//		qrc.setQrcodeErrorCorrect('M');// 15%
//		qrc.setQrcodeVersion(7); // 45*45
//		byte[] contentBytes = content.getBytes();
//		BufferedImage bufimg = new BufferedImage(4500, 4500,BufferedImage.TYPE_INT_RGB);
//		Graphics2D gs = bufimg.createGraphics();
//		// 设置背景颜色
//		gs.setBackground(Color.white);
//		gs.clearRect(0, 0, 4500, 4500);
//		// 设置背景颜色
//		gs.setColor(Color.black);
//		if (contentBytes.length > 120) {
//			System.out.println("字数不能超过120");
//		} else {
//			boolean[][] out = qrc.calQrcode(contentBytes);
//			for (int i = 0; i < out.length; i++)
//				for (int j = 0; j < out.length; j++)
//					if (out[j][i]) {
//						gs.fillRect(j * 100, i * 100, 100, 100);
//					}
//		}
//		// gs.fillRect(0, 0, 100, 100);
//		gs.dispose();
//		bufimg.flush();
//		return bufimg;

		try{
			//设置二维码纠错级别ＭＡＰ
			Hashtable<EncodeHintType, ErrorCorrectionLevel> hintMap = new Hashtable<EncodeHintType, ErrorCorrectionLevel>();
			hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L); // 矫错级别
			//创建比特矩阵

			QRCodeWriter qrCodeWriter = new QRCodeWriter();
			//创建比特矩阵(位矩阵)的QR码编码的字符串
			BitMatrix byteMatrix = qrCodeWriter.encode(content, BarcodeFormat.QR_CODE, 4500, 4500, hintMap);
			// 使BufferedImage勾画QRCode (matrixWidth 是行二维码像素点)
			int matrixWidth = byteMatrix.getWidth();

			BufferedImage image = new BufferedImage(matrixWidth, matrixWidth, BufferedImage.TYPE_INT_RGB);
			image.createGraphics();
			Graphics2D graphics = (Graphics2D) image.getGraphics();
			graphics.setColor(Color.WHITE);
			graphics.fillRect(0, 0, matrixWidth, matrixWidth);
			// 使用比特矩阵画并保存图像
			graphics.setColor(Color.BLACK);
			for (int i = 0; i < matrixWidth; i++){
				for (int j = 0; j < matrixWidth; j++){
					if (byteMatrix.get(i, j)){
					 	graphics.fillRect(i, j, 1, 1);
					}
				}
			}
			return image;
		}catch (Exception ex){
			return null;
		}
	}
}