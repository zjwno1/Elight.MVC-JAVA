package com.elight.utils;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {
	
	/**
	 * 当前日期加上天数后的日期
	 * @param d
	 * @param num
	 * @return
	 */
	public static Date plusDay(Date d, int num) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		cal.add(Calendar.DAY_OF_YEAR, num);
		return cal.getTime();
	}

	/**
	 * 当前日期加上num月的日期
	 * @param d
	 * @param num
	 * @return
	 */
	public static Date plusMonth(Date d, int num) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		cal.add(Calendar.MONTH, num);
		return cal.getTime();
	}
}
