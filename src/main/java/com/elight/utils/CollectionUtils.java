package com.elight.utils;

import java.util.Collection;

public class CollectionUtils {
	public static final CharSequence DEFAULT_JOIN_SEPARATOR = ",";
	/**
	 * is null or its size is 0
	 * 
	 * <pre>
	 * isEmpty(null)   =   true;
	 * isEmpty({})     =   true;
	 * isEmpty({1})    =   false;
	 * </pre>
	 * 
	 * @param <V>
	 * @param c
	 * @return if collection is null or its size is 0, return true, else return
	 *         false.
	 */
	public static <V> boolean isEmpty(Collection<V> c) {
		return (c == null || c.size() == 0);
	}
}
