package com.elight.utils;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.imageio.ImageIO;

public class ImageUtils {
	public static BufferedImage base64String2Image(String imageStr) {
		try {
			byte[] byteArray = Base64Utils.decode(imageStr);
			BufferedImage image = byteToImage(byteArray);
			return image;
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Image转字符串
	 * 
	 * @param image
	 * @return
	 */
	public static String image2Base64String(Image image) {
		return image2Base64String(image2BufferedImage(image));
	}

	/**
	 * Image转字符串
	 * 
	 * @param image
	 * @return
	 */
	public static String image2Base64String(BufferedImage image) {
		try {
			byte[] byteArray = imageToByte(image, "png");
			return Base64Utils.encode(byteArray);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 保存图片
	 * 
	 * @param img
	 * @param savePath
	 *            带文件名 d:/123.png
	 * @param formatName
	 *            格式名 jpg png
	 * @return
	 */
	public static boolean savaImage(BufferedImage image, String savePath,
			String formatName) {
		try {
			ImageIO.write(image, formatName, new File(savePath));
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	/**
	 * 保存图片到磁盘，默认为png格式
	 * 
	 * @param image
	 * @param savePath
	 *            带文件名 d:/123.png
	 * @return
	 */
	public static boolean savaImage(BufferedImage image, String savePath) {
		try {
			ImageIO.write(image, "png", new File(savePath));
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	/**
	 * 将图像字节数组转化为BufferedImage图像实例。
	 * 
	 * @param imageBytes
	 *            图像字节数组。
	 * @return BufferedImage图像实例。
	 * @throws IOException
	 *             IO异常。
	 */
	public static BufferedImage byteToImage(byte[] imageBytes)
			throws IOException {
		ByteArrayInputStream input = new ByteArrayInputStream(imageBytes);
		BufferedImage image = ImageIO.read(input);
		try {
			return image;
		} finally {
			if (input != null) {
				input.close();
			}
		}
	}

	/**
	 * 将BufferedImage持有的图像转化为指定图像格式的字节数组。
	 * 
	 * @param bufferedImage
	 *            图像。
	 * @param formatName
	 *            图像格式名称。
	 * @return 指定图像格式的字节数组。
	 * @throws IOException
	 *             IO异常。
	 */
	public static byte[] imageToByte(BufferedImage bufferedImage,
			String formatName) throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		ImageIO.write(bufferedImage, formatName, output);
		try {
			return output.toByteArray();
		} finally {
			if (output != null) {
				output.close();
			}
		}
	}

	/**
	 * Image转BufferedImage
	 * 
	 * @param image
	 * @return
	 */
	public static BufferedImage image2BufferedImage(Image image) {
		BufferedImage bufferedImage = new BufferedImage(image.getWidth(null),
				image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = bufferedImage.createGraphics();
		g.drawImage(image, null, null);
		g.dispose();
		System.out.println(bufferedImage.getWidth());
		System.out.println(bufferedImage.getHeight());
		return bufferedImage;
	}

	/**
	 * BufferedImage转Image
	 * 
	 * @param image
	 * @return
	 */
	public static Image bufferedImage2Image(BufferedImage img) {
		return (Image) img;
	}
}
