package com.elight.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * URL编码工具类
 * @author zhoujunwen
 * @date 2015-06-26
 */
public class URLEncodeUtils {
	
	public static String encodingUTF8(String str) {
		return encoding(str,"utf-8");
	}
	
	public static String encodingGB2312(String str) {
		return encoding(str,"GB2312");
	}
	
	public static String encoding(String str,String charset){
		try {
			return URLEncoder.encode(str, charset);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public static String decodingUTF8(String str){
		return decoding(str,"utf-8");
	}
	
	
	public static String decodingGB2312(String str){
		return decoding(str,"GB2312");
	}
	
	
	public static String decoding(String str,String charset) {
		try {
			return URLDecoder.decode(str, charset);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "";
	}

}
