package com.elight.utils;

import org.json.JSONObject;
import org.json.XML;

public class JsonUtils {
	public static String xml2Json(String strXml) {
		JSONObject xmlJSONObj = XML.toJSONObject(strXml);
		return xmlJSONObj.toString();
	}
}	
