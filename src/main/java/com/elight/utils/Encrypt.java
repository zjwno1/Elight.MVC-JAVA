package com.elight.utils;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public class Encrypt {
	public static void main(String[] args) {
		System.out.println(MD5Util.MD5(Encrypt.DESEncrypt(MD5Util.MD5("123456"), "juhgtdjc")).toLowerCase());
		
	}
	/// <summary>
	/// 默认密钥。
	/// </summary>
	private static String DESENCRYPT_KEY = "hsdjxlzf";

	/// <summary>
	/// DES加密，使用自定义密钥。
	/// </summary>
	/// <param name="text">待加密的明文</param>
	/// <param name="key">8位字符的密钥字符串</param>
	/// <returns></returns>
	public static String DESEncrypt(String text, String key) {
		try {
			String s = "";
			if (!StringUtils.isNullOrEmpty(text)) {
				// DES算法要求有一个可信任的随机数源
				SecureRandom sr = new SecureRandom();
				// 从原始密钥数据创建DESKeySpec对象
				DESKeySpec desKeySpec = new DESKeySpec(key.getBytes());
				// 创建一个密钥工厂，用它将DESKeySpec转化成SecretKey对象
				SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
				SecretKey securekey = keyFactory.generateSecret(desKeySpec);
				// Cipher对象实际完成加密操作
				Cipher cipher = Cipher.getInstance("DES");
				// 用密钥初始化Cipher对象
				cipher.init(Cipher.ENCRYPT_MODE, securekey, sr);
				// 将加密后的数据编码成字符串
				Base64Utils base64Utils = new Base64Utils();
				s = base64Utils.encode(cipher.doFinal(text.getBytes()));
			}
			return s;
		} catch (Exception e) {
			return "";
		}
	}


	/// <summary>
	/// DES解密，使用自定义密钥。
	/// </summary>
	/// <param name="cyphertext">待解密的秘文</param>
	/// <param name="key">必须是8位字符的密钥字符串(不能有特殊字符)</param>
	/// <returns></returns>
	public static String DESDecrypt(String cyphertext, String key) {
		try {
			String s = "";  
	        if (!StringUtils.isNullOrEmpty(cyphertext) )  
	        {  
	            //DES算法要求有一个可信任的随机数源  
	            SecureRandom sr = new SecureRandom();  
	            //从原始密钥数据创建DESKeySpec对象  
	            DESKeySpec desKeySpec = new DESKeySpec(key.getBytes());  
	            //创建一个密钥工厂，用它将DESKeySpec转化成SecretKey对象  
	            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");  
	            SecretKey securekey = keyFactory.generateSecret(desKeySpec);  
	            //Cipher对象实际完成解密操作  
	            Cipher cipher = Cipher.getInstance("DES");  
	            //用密钥初始化Cipher对象  
	            cipher.init(Cipher.DECRYPT_MODE, securekey, sr);  
	            //将加密后的数据解码再解密  
	            Base64Utils base64Utils = new Base64Utils();  
	            byte[] buf = cipher.doFinal(base64Utils.decode(cyphertext));  
	            s = new String(buf);  
	        }  
	        return s;  
		} catch (Exception e) {
			return "";
		}

	}

	/// <summary>
	/// DES加密，使用默认密钥。
	/// </summary>
	/// <param name="text">待加密的明文</param>
	/// <returns></returns>
	public static String DESEncrypt(String text) {
		return DESEncrypt(text, DESENCRYPT_KEY);
	}

	/// <summary>
	/// DES解密，使用默认密钥。
	/// </summary>
	/// <param name="cyphertext">待解密的秘文</param>
	/// <returns></returns>
	public static String DESDecrypt(String cyphertext) {
		return DESDecrypt(cyphertext, DESENCRYPT_KEY);
	}
}
