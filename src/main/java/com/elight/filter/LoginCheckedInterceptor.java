package com.elight.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.elight.entity.Operator;
import com.elight.utils.OperatorProvider;

/**
 * 登录验证过滤器
 * @author zhoujunwen
 *
 */
public class LoginCheckedInterceptor extends HandlerInterceptorAdapter {
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		if (handler.getClass().isAssignableFrom(HandlerMethod.class)) {
			LoginChecked loginChecked = ((HandlerMethod) handler).getMethodAnnotation(LoginChecked.class);
			// 没有声明需要权限,或者声明不验证权限
			if(loginChecked == null)
				return true;
			if(loginChecked.ignore()) {
				return true;
			}
			Operator operator = OperatorProvider.getCurrent(request.getSession());
            if (operator == null)
            {
            	response.sendRedirect(request.getContextPath()+"/Account/Login");
				return false;
            }
			return true;
		}
		return true;
	}
}
