<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/ico" href="${pageContext.request.contextPath}//favicon.ico" />
    <link rel="bookmark" type="image/ico" href="${pageContext.request.contextPath}//favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link href="${pageContext.request.contextPath}/Content/layui/css/layui.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/select2/css/select2.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/framework/css/console.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/framework/css/animate.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <script src="${pageContext.request.contextPath}/Content/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/Content/layui/layui.js"></script>
    <script src="${pageContext.request.contextPath}/Content/select2/js/select2.min.js"></script>
    <script src="${pageContext.request.contextPath}/Content/framework/js/global.js"></script>
</head>
<form id="form" class="layui-form" style="margin-top: 25px">
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">账号</label>
            <div class="layui-input-inline">
                <input type="hidden" name="Id" disabled />
                <input type="text" name="account" id="account" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">初始密码</label>
            <div class="layui-input-inline">
                <input type="password" name="password" id="password" autocomplete="off" class="layui-input" disabled>
            </div>
        </div>

    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">昵称</label>
            <div class="layui-input-inline">
                <input type="text" name="nickName" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">姓名</label>
            <div class="layui-input-inline">
                <input type="text" name="realName" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>

    </div>

    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">出生日期</label>
            <div class="layui-input-inline">
                <input class="layui-input" id="strBirthDay" name="strBirthDay"  disabled />
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">性别</label>
            <div class="layui-input-inline">
                <input type="radio" name="gender" value="1" title="男" checked  disabled/>
                <input type="radio" name="gender" value="0" title="女"  disabled/>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">邮箱</label>
            <div class="layui-input-inline">
                <input type="text" name="email" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">所属机构</label>
            <div class="layui-input-inline">
                <select lay-ignore name="departmentId" id="departmentId" style="width: 190px" class="select2" disabled>
                </select>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">角色属性</label>
            <div class="layui-input-inline">
                <select lay-ignore name="roleId" class="select2" id="RoleId" multiple="multiple" style="width: 514px;"  disabled>
                </select>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">移动电话</label>
            <div class="layui-input-inline">
                <input type="text" name="mobilePhone" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">联系地址</label>
            <div class="layui-input-inline">
                <input type="text" name="address" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">排序码</label>
            <div class="layui-input-inline">
                <input type="number" name="sortCode" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">启用</label>
            <div class="layui-input-inline">
                <input type="radio" name="isEnabled" value="1" title="是" checked disabled/>
                <input type="radio" name="isEnabled" value="0" title="否" disabled/>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
       <div class="layui-inline">
            <label class="layui-form-label label-required">客户URL</label>
            <div class="layui-input-inline">
                <input type="text" name="clientUrl" style="width:300px;" placeholder="请输入客户URL" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">个性签名</label>
            <div class="layui-input-inline" style="width: 514px;">
                <textarea name="signature" class="layui-textarea" disabled placeholder="这位同学很懒，木有签名的说～"></textarea>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">创建人员</label>
            <div class="layui-input-inline">
                <input type="text" name="createUser" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">创建时间</label>
            <div class="layui-input-inline">
                <input type="text" name="createTime" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">修改人员</label>
            <div class="layui-input-inline">
                <input type="text" name="modifyUser" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">修改时间</label>
            <div class="layui-input-inline">
                <input type="text" name="modifyTime" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
    </div>
</form>
<script>
    layui.use(['element', 'form', 'laydate'], function () {
        var form = layui.form;
        var element = layui.element;
        var laydate = layui.laydate;

        laydate.render({
            elem: "#strBirthDay"
        });

        $("#departmentId").bindSelect({
            url: "${pageContext.request.contextPath}/System/Organize/GetListTreeSelect"
        });

        $("#roleId").bindSelect({
            url: "${pageContext.request.contextPath}/System/Role/GetListTreeSelect"
        });

        var primaryKey = $.getQueryString("primaryKey");
        if (primaryKey) {
            $.ajax({
                url: "${pageContext.request.contextPath}/System/User/GetForm",
                data: { primaryKey: primaryKey },
                type: "post",
                dataType: "json",
                async: false,
                success: function (data) {
                    $("#form").formSerialize(data);
                }
            });
        }

        form.render();

    });
</script>
</body>
</html>