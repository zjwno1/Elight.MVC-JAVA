<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/ico" href="${pageContext.request.contextPath}/Content/favicon.ico" />
    <link rel="bookmark" type="image/ico" href="${pageContext.request.contextPath}/Content/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link href="${pageContext.request.contextPath}/Content/layui/css/layui.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/select2/css/select2.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/framework/css/console.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/framework/css/animate.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <script src="${pageContext.request.contextPath}/Content/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/Content/layui/layui.js"></script>
    <script src="${pageContext.request.contextPath}/Content/select2/js/select2.min.js"></script>
    <script src="${pageContext.request.contextPath}/Content/framework/js/global.js"></script>
</head>
<body>
<form id="form" class="layui-form" style="margin-top: 25px">
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label label-required">账号</label>
            <div class="layui-input-inline">
                <input type="hidden" name="id" />
                <input type="text" name="account" id="account" lay-verify="required" placeholder="请输入账号" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label label-required">初始密码</label>
            <div class="layui-input-inline">
                <input type="password" name="password" id="password" lay-verify="required" placeholder="请输入密码" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label label-required">昵称</label>
            <div class="layui-input-inline">
                <input type="text" name="nickName" lay-verify="required" placeholder="请输入昵称" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label label-required">姓名</label>
            <div class="layui-input-inline">
                <input type="text" name="realName" lay-verify="required" placeholder="请输入姓名" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label label-required">出生日期</label>
            <div class="layui-input-inline">
                <input class="layui-input" id="strBirthDay" name="strBirthDay" lay-verify="required|date" placeholder="请选择日期"  />
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label label-required">性别</label>
            <div class="layui-input-inline">
                <input type="radio" name="gender" value="1" title="男" checked />
                <input type="radio" name="gender" value="0" title="女" />
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label label-required">邮箱</label>
            <div class="layui-input-inline">
                <input type="text" name="email" placeholder="请输入邮箱" lay-verify="required|email" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label label-required">所属机构</label>
            <div class="layui-input-inline">
                <select lay-ignore name="departmentId" id="departmentId" lay-verify="required" style="width: 190px" class="select2"></select>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label label-required">角色属性</label>
            <div class="layui-input-inline">
                <select lay-ignore name="roleId" class="select2" id="roleId" multiple="multiple" style="width: 514px;" lay-verify="required"></select>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label label-required">移动电话</label>
            <div class="layui-input-inline">
                <input type="text" name="mobilePhone" lay-verify="required|phone" placeholder="请输入手机" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label label-required">联系地址</label>
            <div class="layui-input-inline">
                <input type="text" name="address" lay-verify="required" placeholder="请输入地址" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label label-required">排序码</label>
            <div class="layui-input-inline">
                <input type="number" name="sortCode" lay-verify="number" placeholder="请输入排序" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label label-required">启用</label>
            <div class="layui-input-inline">
                <input type="radio" name="isEnabled" value="1" title="是" checked />
                <input type="radio" name="isEnabled" value="0" title="否" />
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label label-required">客户URL</label>
            <div class="layui-input-inline">
                <input type="text" name="clientUrl" style="width:300px;" placeholder="请输入客户URL" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">个性签名</label>
            <div class="layui-input-inline" style="width: 514px;">
                <textarea name="signature" placeholder="请输入个性签名" class="layui-textarea"></textarea>
            </div>
        </div>
    </div>
    <div class="layui-form-item" style="display: none">
        <div class="layui-input-block">
            <button id="btnSubmit" class="layui-btn" lay-submit lay-filter="add">提交</button>
        </div>
    </div>
</form>

<script src="${pageContext.request.contextPath}/Content/jquery/jquery.md5.js"></script>
<script>
    layui.use(['element', 'form', 'laydate', 'layer'], function () {
        var form = layui.form;
        var element = layui.element;
        var laydate = layui.laydate;

        laydate.render({
            elem: "#strBirthDay"
        });

        $("#departmentId").bindSelect({
            url: "${pageContext.request.contextPath}/System/Organize/GetListTreeSelect"
        });

        $("#roleId").bindSelect({
            url: "${pageContext.request.contextPath}/System/Role/GetListTreeSelect",
            title: '请选择角色'
        });

        $('#account').blur(function () {
            var userName = $('#account').val();
            if (userName && userName.length > 0) {
                $.formSubmit({
                    url: '${pageContext.request.contextPath}/System/User/CheckAccount',
                    data: { userName: userName },
                    close: false
                });
            }
            return false;
        });

        var primaryKey = $.getQueryString("primaryKey");
        if (primaryKey) {
            $('#account').attr('disabled', true).addClass('layui-disabled');
            $('#password').attr('disabled', true).val('******').addClass('layui-disabled');
            $.ajax({
                url: "${pageContext.request.contextPath}/System/User/GetForm",
                data: { primaryKey: primaryKey },
                type: "post",
                dataType: "json",
                async: false,
                success: function (data) {
                    $("#form").formSerialize(data);
                }
            });
        }

        form.render();

        form.on('submit(add)', function (form) {
            //角色信息。
            form.field.roleIds = $("#roleId").val().join();
            //密码加密。
            form.field.password = $.md5(form.field['password']);
            $.formSubmit({
                url: "${pageContext.request.contextPath}/System/User/Form",
                data: form.field
            });
            return false;
        });

    });
</script>
</body>
</html>