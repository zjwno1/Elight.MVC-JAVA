<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/ico" href="${pageContext.request.contextPath}/favicon.ico" />
    <link rel="bookmark" type="image/ico" href="${pageContext.request.contextPath}/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link href="${pageContext.request.contextPath}/Content/layui/css/layui.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/select2/css/select2.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/framework/css/console.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/framework/css/animate.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <script src="${pageContext.request.contextPath}/Content/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/Content/layui/layui.js"></script>
    <script src="${pageContext.request.contextPath}/Content/select2/js/select2.min.js"></script>
    <script src="${pageContext.request.contextPath}/Content/framework/js/global.js"></script>
</head>
<form id="form" class="layui-form" style="margin-top: 25px">
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label label-required">编码</label>
            <div class="layui-input-inline">
                <input type="hidden" name="id" />
                <input type="text" name="enCode" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">名称</label>
            <div class="layui-input-inline">
                <input type="text" name="fullName" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">上级</label>
            <div class="layui-input-inline">
                <select class="select2" lay-ignore name="parentId" id="parentId" style="width: 190px">
                </select>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">类型</label>
            <div class="layui-input-inline">
                <select name="type">
                    <option value=""></option>
                    <option value="0">公司</option>
                    <option value="1">部门</option>
                    <option value="2">小组</option>
                </select>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">主管</label>
            <div class="layui-input-inline">
                <input type="text" name="managerId" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">联系电话</label>
            <div class="layui-input-inline">
                <input type="text" name="telePhone" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">微信</label>
            <div class="layui-input-inline">
                <input type="text" name="weChat" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">传真</label>
            <div class="layui-input-inline">
                <input type="text" name="fax" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">

        <div class="layui-inline">
            <label class="layui-form-label">邮箱</label>
            <div class="layui-input-inline">
                <input type="text" name="email" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">地址</label>
            <div class="layui-input-inline">
                <input type="text" name="address" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">排序码</label>
            <div class="layui-input-inline">
                <input type="number" name="sortCode" placeholder="请输入排序" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">启用</label>
            <div class="layui-input-inline">
                <input type="radio" name="isEnabled" value="1" title="是" checked />
                <input type="radio" name="isEnabled" value="0" title="否" />
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">备注</label>
        <div class="layui-input-block" style="width: 514px;">
            <textarea name="remark" class="layui-textarea"></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">创建人员</label>
            <div class="layui-input-inline">
                <input type="text" name="createUser" autocomplete="off" class="layui-input" />
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">创建时间</label>
            <div class="layui-input-inline">
                <input type="text" name="createTime" autocomplete="off" class="layui-input" />
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">修改人员</label>
            <div class="layui-input-inline">
                <input type="text" name="modifyUser" autocomplete="off" class="layui-input" />
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">修改时间</label>
            <div class="layui-input-inline">
                <input type="text" name="modifyTime" autocomplete="off" class="layui-input" />
            </div>
        </div>
    </div>

</form>
<script>
    layui.use(['form', 'layer'], function () {
        var form = layui.form;
        var layer = layui.layer;
        $("#parentId").bindSelect({
            url: "${pageContext.request.contextPath}/System/Organize/GetListTreeSelect"
        });

        var primaryKey = $.getQueryString("primaryKey");
        if (primaryKey) {
            $.ajax({
                url: "${pageContext.request.contextPath}/System/Organize/GetForm",
                data: { primaryKey: primaryKey },
                type: "post",
                dataType: "json",
                async: false,
                success: function (data) {
                    $("#form").formSerialize(data);
                }
            });
        }

        form.render();

    });

    function loadFormData() {
       
    }

</script>


</body>
</html>