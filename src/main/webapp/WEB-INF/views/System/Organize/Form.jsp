<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/ico" href="${pageContext.request.contextPath}/Content/favicon.ico" />
    <link rel="bookmark" type="image/ico" href="${pageContext.request.contextPath}/Content/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link href="${pageContext.request.contextPath}/Content/layui/css/layui.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/select2/css/select2.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/framework/css/console.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/framework/css/animate.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <script src="${pageContext.request.contextPath}/Content/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/Content/layui/layui.js"></script>
    <script src="${pageContext.request.contextPath}/Content/select2/js/select2.min.js"></script>
    <script src="${pageContext.request.contextPath}/Content/framework/js/global.js"></script>
</head>
<body>
<form id="form" class="layui-form" style="margin-top: 25px">
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label label-required">编码</label>
            <div class="layui-input-inline">
                <input type="hidden" name="id" />
                <input type="text" name="enCode" lay-verify="required" placeholder="请输入编码" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">名称</label>
            <div class="layui-input-inline">
                <input type="text" name="fullName" lay-verify="required" placeholder="请输入名称" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">上级</label>
            <div class="layui-input-inline">
                <select class="select2" lay-ignore name="parentId" lay-verify="required" id="parentId" style="width: 190px">
                </select>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">类型</label>
            <div class="layui-input-inline">
                <select name="type" lay-verify="required">
                    <option value=""></option>
                    <option value="0">公司</option>
                    <option value="1">部门</option>
                    <option value="2">小组</option>
                </select>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">主管</label>
            <div class="layui-input-inline">
                <input type="text" name="managerId" placeholder="请输入主管" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">联系电话</label>
            <div class="layui-input-inline">
                <input type="text" name="telePhone" placeholder="请输入电话" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">微信</label>
            <div class="layui-input-inline">
                <input type="text" name="weChat" placeholder="请输入微信" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">传真</label>
            <div class="layui-input-inline">
                <input type="text" name="fax" placeholder="请输入传真" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>

    <div class="layui-form-item">

        <div class="layui-inline">
            <label class="layui-form-label">邮箱</label>
            <div class="layui-input-inline">
                <input type="text" name="email" placeholder="请输入邮箱" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">地址</label>
            <div class="layui-input-inline">
                <input type="text" name="address" placeholder="请输入地址" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">排序码</label>
            <div class="layui-input-inline">
                <input type="number" name="sortCode" lay-verify="number" placeholder="请输入排序" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">启用</label>
            <div class="layui-input-inline">
                <input type="radio" name="isEnabled" value="1" title="是" checked />
                <input type="radio" name="isEnabled" value="0" title="否" />
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">备注</label>
        <div class="layui-input-block" style="width: 514px;">
            <textarea name="remark" placeholder="请输入备注" class="layui-textarea"></textarea>
        </div>
    </div>
    <div class="layui-form-item" style="display: none">
        <div class="layui-input-block">
            <button id="btnSubmit" class="layui-btn" lay-submit lay-filter="add">提交</button>
        </div>
    </div>
</form>
<script>
    layui.use(['form', 'layer'], function () {
        var form = layui.form;
        var layer = layui.layer;

        $("#parentId").bindSelect({
            url: "${pageContext.request.contextPath}/System/Organize/GetListTreeSelect"
        });
        
        var primaryKey = $.getQueryString("primaryKey");
        if (primaryKey) {
            $.ajax({
                url: "${pageContext.request.contextPath}/System/Organize/GetForm",
                data: { primaryKey: primaryKey },
                type: "post",
                dataType: "json",
                async: false,
                success: function (data) {
                    $("#form").formSerialize(data);
                }
            });
        }

        form.render();

        form.on('submit(add)', function (data) {
            $.formSubmit({
                url: "${pageContext.request.contextPath}/System/Organize/Form",
                data: data.field
            });
            return false;
        });

    });
</script>


</body>
</html>