<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/ico" href="${pageContext.request.contextPath}/Content/favicon.ico" />
    <link rel="bookmark" type="image/ico" href="${pageContext.request.contextPath}/Content/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link href="${pageContext.request.contextPath}/Content/layui/css/layui.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/select2/css/select2.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/framework/css/console.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/framework/css/animate.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <script src="${pageContext.request.contextPath}/Content/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/Content/layui/layui.js"></script>
    <script src="${pageContext.request.contextPath}/Content/select2/js/select2.min.js"></script>
    <script src="${pageContext.request.contextPath}/Content/framework/js/global.js"></script>
</head>
<body>

<form id="form" class="layui-form" style="margin-top: 25px">
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">编码</label>
            <div class="layui-input-inline">
                <input type="hidden" name="id" />
                <input type="text" name="enCode" lay-verify="required" placeholder="请输入编码" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">名称</label>
            <div class="layui-input-inline">
                <input type="text" name="name" lay-verify="required" placeholder="请输入名称" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">父级</label>
            <div class="layui-input-inline">
                <select class="select2" lay-ignore name="parentId" lay-verify="required" id="parentId" style="width: 190px">
                </select>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">连接</label>
            <div class="layui-input-inline">
                <input type="text" name="url" placeholder="请输入连接" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">类型</label>
            <div class="layui-input-inline">
                <select name="type" lay-verify="required">
                    <option value=""></option>
                    <option value="0">菜单</option>
                    <option value="1">按钮</option>
                    <option value="2">其他</option>
                </select>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">事件</label>
            <div class="layui-input-inline">
                <input type="text" name="jsEvent" placeholder="请输入事件" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">图标</label>
            <div class="layui-input-inline">
                <input type="text" name="icon" id="icon" placeholder="请选择图标" autocomplete="off" class="layui-input">
            </div>
            <button class="layui-btn layui-btn-primary" id="btnSetIcon" style="position: absolute; left: 244px;">
                <i class="layui-icon">&#xe615;</i>
            </button>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">排序码</label>
            <div class="layui-input-inline">
                <input type="number" name="sortCode" lay-verify="number" placeholder="请输入排序" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">选项</label>
        <div class="layui-input-block">
            <input type="checkbox" lay-skin="primary" name="isEnable" value="true" title="启用" checked>
            <input type="checkbox" lay-skin="primary" name="isEdit" value="true" title="编辑">
            <input type="checkbox" lay-skin="primary" name="isPublic" value="true" title="公共">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">备注</label>
        <div class="layui-input-block" style="width: 514px;">
            <textarea name="remark" placeholder="请输入内容" class="layui-textarea"></textarea>
        </div>
    </div>
    <div class="layui-form-item" style="display: none">
        <div class="layui-input-block">
            <button id="btnSubmit" class="layui-btn" lay-submit lay-filter="add">提交</button>
        </div>
    </div>
</form>
<script>
    layui.use(['form', 'layer'], function () {
        var form = layui.form;
        var layer = layui.layer;
        $("#parentId").bindSelect({
            url: "${pageContext.request.contextPath}/System/Permission/GetParent"
        });
        var primaryKey = $.getQueryString("primaryKey");
        if (primaryKey) {
            $.ajax({
                url: "${pageContext.request.contextPath}/System/Permission/GetForm",
                data: { primaryKey: primaryKey },
                type: "post",
                dataType: "json",
                async: false,
                success: function (data) {
                    $("#form").formSerialize(data);
                }
            });
        }

        form.render();

        form.on('submit(add)', function (data) {
            $.formSubmit({
                url: "${pageContext.request.contextPath}/System/Permission/Form",
                data: data.field
            });
            return false;
        });

        $("#btnSetIcon").click(function () {
            $.layerOpen({
                id: "icon",
                title: "设置图标",
                width: "800px",
                height: "580px",
                maxmin: true,
                content: "${pageContext.request.contextPath}/System/Permission/Icon",
                yes: function (body,win, index) {
                    $('#icon').val(win.className);
                    top.layer.close(index);
                }
            });
            return false;
        });
    });

</script>

</body>
</html>