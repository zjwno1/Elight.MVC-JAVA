<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>
<head>
    <title>Index</title>
    <meta name="viewport" content="width=device-width" />
    <link href="${pageContext.request.contextPath}/Content/layui/css/layui.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/framework/css/console.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/framework/css/animate.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <script src="${pageContext.request.contextPath}/Content/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/Content/layui/layui.js"></script>
    <script src="${pageContext.request.contextPath}/Content/framework/js/global.js"></script>
</head>
<body>
    <div class="panel animated fadeIn">
        <div class="panel-body">
            <div id="toolbar" class="elight-table-toolbar">
                <div class="layui-btn-group"></div>
                <button id="btnSearch" class="toolbar-search-button layui-btn layui-btn-normal layui-btn-small">
                    <i class="layui-icon">&#xe615;</i>
                </button>
                <div class="toolbar-search-input">
                    <input type="text" id="keyWord" placeholder="权限名称或编码" autocomplete="off" class="layui-input">
                </div>
            </div>
            <table id="gridList" class="layui-form layui-table elight-table" lay-skin="line">
                <thead>
                    <tr>
                        <th>
                            <input type="checkbox" lay-skin="primary">
                        </th>
                        <th>编码</th>
                        <th>图标</th>
                        <th>名称</th>
                        <th>连接</th>
                        <th>类型</th>
                        <th>排序码</th>
                        <th>公共</th>
                        <th>状态</th>
                        <th>备注</th>
                    </tr>
                </thead>
                <!--内容容器-->
                <tbody id="content"></tbody>
            </table>
            <div id="paged"></div>
        </div>
    </div>
</body>
</html>

<!--内容模板-->
<script id="contentTpl" type="text/html">
    {{#  layui.each(d.list, function(index, item){ }}
    <tr>
        <td>
            <input type="checkbox" lay-skin="primary" value="{{item.id}}">
        </td>
        <td>
            {{# if(item.enCode==null){ }}
            {{# }else{ }}{{item.enCode}}
            {{# } }}
        </td>
        <td><i class="{{item.icon}}"></i></td>
        <td>{{item.name}}</td>
        <td>
            {{# if(item.url==null){ }}无
            {{# }else{ }}{{item.url}}
            {{# } }}
        </td>
        <td>
            {{# if(item.type==0){ }} 菜单
            {{# }else if(item.type==1){ }} 按钮
            {{# }else{ }} 其他
            {{# } }}
        </td>
        <td>{{item.sortCode}}</td>
        <td>
            {{# if(item.isPublic==true){ }}  <span class="label label-success label-sm">是</span>
            {{# }else{ }}  <span class="label label-dark label-sm">否</span>
            {{# } }}
        </td>
        <td>
            {{# if(item.isEnable==true){ }}  <span class="label label-success label-sm">启用</span>
            {{# }else{ }}  <span class="label label-dark label-sm">禁用</span>
            {{# } }}
        </td>
        <td>
            {{# if(item.Remark==null){ }}无
            {{# }else{ }}{{item.remark}}
            {{# } }}
        </td>
    </tr>
    {{#  }); }}
    {{# if(d.list.length<=0) { }}
    <tr style="color: red">
        <td colspan="10">查无数据。</td>
    </tr>
    {{# } }}
</script>

<script type="text/javascript">
    var paging;
    layui.config({
        base: parent._baseUrl
    }).use(['paging', 'form'], function () {
        var form = layui.form;
        paging = layui.paging();
        initGrid();
        $("#toolbar").authorizeButton();
        $('#btnSearch').click(initGrid);
        $('#keyWord').bindEnterEvent(initGrid);
    });

    function initGrid() {
        paging.init({
            url: '${pageContext.request.contextPath}/System/Permission/Index',
            elem: '#content',
            tempElem: '#contentTpl',
            params: {
                keyWord: $("#keyWord").val()
            },
            pageConfig: {
                elem: 'paged',
                pageSize: 10
            },
            success: function () {
            },
            fail: function (msg) {
                top.layer.msg(msg);
            }
        });
    }

    function btn_add() {
        $.layerOpen({
            id: "add",
            title: "新增权限",
            width: "670px",
            height: "530px",
            content: "${pageContext.request.contextPath}/System/Permission/Form",
            yes: function (iBody) {
                iBody.find('#btnSubmit').click();
                initGrid();
            }
        });
    }

    function btn_edit() {
        var ids = $("#gridList").gridSelectedRowValue();
        if (ids.length != 1) {
            $.layerMsg("请勾选单条记录修改。", "warning");
            return;
        }
        $.layerOpen({
            id: "edit",
            title: "修改权限",
            width: "670px",
            height: "530px",
            content: "${pageContext.request.contextPath}/System/Permission/Form?primaryKey=" + ids[0],
            yes: function (iBody) {
                iBody.find('#btnSubmit').click();
                initGrid();
            }
        });
    }

    function btn_delete() {
        var ids = $("#gridList").gridSelectedRowValue();
        if (ids.length < 1) {
            $.layerMsg("请勾选需要删除的权限。", "warning");
            return;
        }
        $.layerConfirm({
            content: "您已选中" + ids.length + "条数据, 确定删除吗？",
            callback: function () {
                $.formSubmit({
                    url: '${pageContext.request.contextPath}/System/Permission/Delete',
                    data: { primaryKey: ids[0] },
                    success: function () {
                        initGrid();
                    }
                });
            }
        });
    }

    function btn_detail() {
        var ids = $("#gridList").gridSelectedRowValue();
        if (ids.length != 1) {
            $.layerMsg("请勾选单条记录查看。", "warning");
            return;
        }
        $.layerOpen({
            id: "detail",
            title: "查看权限",
            width: "670px",
            height: "530px",
            content: "${pageContext.request.contextPath}/System/Permission/Detail?primaryKey=" + ids[0],
            btn: null
        });
    }

</script>
