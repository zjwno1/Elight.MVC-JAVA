<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/ico" href="${pageContext.request.contextPath}/Content/favicon.ico" />
    <link rel="bookmark" type="image/ico" href="${pageContext.request.contextPath}/Content/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link href="${pageContext.request.contextPath}/Content/layui/css/layui.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/select2/css/select2.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/framework/css/console.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/framework/css/animate.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <script src="${pageContext.request.contextPath}/Content/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/Content/layui/layui.js"></script>
    <script src="${pageContext.request.contextPath}/Content/select2/js/select2.min.js"></script>
    <script src="${pageContext.request.contextPath}/Content/framework/js/global.js"></script>
</head>
<body>

<div class="alert alert-warning">
    提醒：日志删除后无法恢复，请谨慎操作。
</div>
<form id="form" class="layui-form" style="margin: 20px">
    <div class="layui-form-item">
        <div class="layui-inline">
            <input type="radio" name="keepType" value="30" title="保留最近1个月">
            <input type="radio" name="keepType" value="7" title="保留最近7天">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <input type="radio" name="keepType" value="90" title="保留最近3个月">
            <input type="radio" name="keepType" value="0" title="全部清空">
        </div>
    </div>
    <div class="layui-form-item" style="display: none">
        <div class="layui-input-inline">
            <button id="btnSubmit" class="layui-btn" lay-submit lay-filter="add">提交</button>
        </div>
    </div>
</form>

<script type="text/javascript">
    layui.use('form', function () {
        var form = layui.form;

        form.on('submit(add)', function (data) {
            $.formSubmit({
                url: "${pageContext.request.contextPath}/System/Log/Delete",
                data: data.field
            });
            return false;
        });
    });
</script>

</body>
</html>