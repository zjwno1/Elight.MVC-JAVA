<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<style>
.elight-product-box .product-desc li {
	font-size: 20px;
}

html, body {
	height: 100%;
	width: 100%;
	overflow: hidden;
	margin: 0;
	padding: 0;
	background-color: rgba(53, 51, 51, 0.5);
}

html, body:before {
	background:
		url(${pageContext.request.contextPath}/Content/framework/images/login.jpg);
	background-size: 100% 100%;
	-moz-background-size: 100% 100%;
	-webkit-background-size: 100% 100%;
}

.layui-form-checked[lay-skin=primary] i {
	border-color: #34A8FF;
	background-color: #34A8FF;
}

.elight-login-box {
	position: absolute;
	right: 0px;
	top: 0px;
	width: 450px;
	padding-top: 120px;
	height: inherit;
	background-color: #fff;
	color: aliceblue;
	box-shadow: 0px 0px 5px rgba(53, 51, 51, 0.5);
}

@media screen and (max-width: 768px) {
	.elight-login-box {
		width: 100%;
		padding-top: 20%;
		height: inherit;
		background-color: #fff;
		color: aliceblue;
		box-shadow: 0px 0px 5px rgba(53, 51, 51, 0.5);
	}
	.elight-pull-right {
		float: left !important;
		width: 100%;
		height: 100%;
	}
}

.elight-login-box .elight-login-header {
	font-family: Helvetica, Arial, "Microsoft YaHei", FreeSans, Arimo,
		"Droid Sans";
	color: #334054;
	font-weight: bold;
	font-size: 22px;
	text-align: center;
	width: 100%;
}

.elight-login-box .elight-login-body {
	height: 185px;
	padding: 30px 90px 0;
}

.elight-login-box .elight-login-body .layui-form-item {
	position: relative;
}

.elight-login-box .elight-login-body .layui-form-item .login-icon {
	position: absolute;
	color: #cccccc;
	top: 10px;
	left: 10px;
}

.elight-login-box .elight-login-body .layui-form-item input {
	padding-left: 34px;
	color: #929191;
}

.elight-pull-left {
	float: left !important;
	height: 100%;
}

.elight-pull-right {
	float: right !important;
	height: 100%;
}

.elight-login-box .elight-login-body .login-remember {
	line-height: 38px;
	color: #777;
}

.elight-login-box .elight-login-body .login-code-box img {
	cursor: pointer;
	position: absolute;
	right: 2px;
	top: 5px;
}

.btn-submit {
	width: 100%;
	background-color: #0099CC;
}

.elight-product-box {
	padding-right: 475px;
	padding-left: 70px;
	padding-top: 150px;
	text-align: justify;
	max-width: 100%;
}

.elight-product-box h2 {
	font-size: 25px;
	font-weight: 500;
	margin-bottom: 30px;
	color: #fff;
}

.elight-product-box .product-desc li {
	color: #fff;
	list-style: disc;
	margin-top: 7px;
	max-width: 650px;
}

.elight-product-box .product-btns {
	margin-top: 20px;
}
</style>
<html>
<head>
<meta name="viewport" content="width=device-width" />
<title>${SoftwareName}</title>
<meta name="keywords" content="elight,elight.mvc官网,elight.mvc下载,elight.mvc框架,轻量级开发框架,通用后台管理系统">
<meta name="description" content="Elight.MVC是一款基于Web的通用管理系统轻量级解决方案">
<link rel="shortcut icon" type="image/ico" href="${pageContext.request.contextPath}/Content/favicon.ico" />
<link rel="bookmark" type="image/ico" href="${pageContext.request.contextPath}/Content/favicon.ico" />
<link href="${pageContext.request.contextPath}/Content/layui/css/layui.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/Content/framework/css/animate.css" rel="stylesheet" />
</head>
<body>
	<div class="elight-product-box animated fadeInUp">
		<h2>Elight.MVC通用管理系统轻量级解决方案</h2>
		<ul class="product-desc">
			<li>Elight.MVC是一套基于 ASP.NET MVC5 + Layui开发的通用管理系统快速开发框架。</li>
			<li>本代码由Elight.MVC改写至Java平台,使用Spring MVC + Layui开发的通用管理系统快速开发框架。</li>
			<li>支持SQL Server、MySQL、PostgreSQL和Oracle等多种数据库类型。</li>
			<li>该解决方案适用于OA、电商平台、CRM、物流管理、教务管理等各类管理系统开发。</li>
			<li>兼容除IE8以下所有浏览器，暂不支持移动端。</li>
			<li>初始用户名：admin 密码：123456</li>
		</ul>
		<div class="product-btns">
			<a class="layui-btn layui-btn-normal layui-btn-small" target="_blank" href="http://www.cnblogs.com/esofar/p/7093511.html"><i class="layui-icon">&#xe609;</i>&nbsp;框架介绍</a>
			<a class="layui-btn layui-btn-warm layui-btn-small" target="_blank" href="https://github.com/esofar/elight.mvc"><i class="layui-icon">&#xe601;</i>&nbsp;贡献代码</a>
			<a class="layui-btn layui-btn-danger layui-btn-small" target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=973095739&site=qq&menu=yes"><i class="layui-icon">&#xe606;</i>&nbsp;联系作者</a>
		</div>
	</div>
	<div class="elight-login-box animated fadeInRight">
		<div class="elight-login-header">${SoftwareName}</div>
		<div class="elight-login-body">
			<form class="layui-form">
				<div class="layui-form-item">
					<label class="login-icon"><i class="layui-icon">&#xe612;</i></label>
					<input type="text" name="userName" lay-verify="required"
						   autocomplete="off" placeholder="账号/已认证邮箱" class="layui-input">
				</div>
				<div class="layui-form-item">
					<label class="login-icon"> <i class="layui-icon">&#xe642;</i>
					</label> <input type="password" name="password" lay-verify="required"
									autocomplete="off" placeholder="登陆密码" class="layui-input">
				</div>
				<div class="layui-form-item">
					<div class="login-code-box">
						<label class="login-icon"><i class="layui-icon">&#xe62d;</i></label>
						<input type="text" name="verifyCode" lay-verify="required"
							   autocomplete="off" placeholder="验证码" class="layui-input">
						<img id="verifyCode"
							 src="${pageContext.request.contextPath}/Account/VerifyCode"
							 title="点击更换验证码">
					</div>
					<input class="elight-pull-right" type="checkbox" lay-skin="primary"
						   name="isSaveAccount" value="true" title="记住账号" />
				</div>
				<div class="layui-form-item">
					<button class="layui-btn btn-submit" lay-submit lay-filter="login">立即登录</button>
				</div>
			</form>
		</div>
		<div class="elight-login-fooder"></div>
	</div>
</body>
</html>
<script src="${pageContext.request.contextPath}/Content/jquery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/Content/jquery/jquery.md5.js"></script>
<script src="${pageContext.request.contextPath}/Content/framework/js/global.js"></script>
<script src="${pageContext.request.contextPath}/Content/layui/layui.js"></script>
<script>
layui.use(['layer', 'form'], function () {
    //var $ = layui.jquery;
    var layer = layui.layer;
    var form = layui.form;
    form.on('submit(login)', function (data) {
        data.field.password = $.md5(data.field.password);
        $(".btn-submit").html("正在登录...");
        $(".btn-submit").attr('disabled', true).addClass('layui-disabled');
        $.ajax({
            url: "${pageContext.request.contextPath}/Account/Login",
            data: data.field,
            type: "post",
            dataType: "json",
            success: function (result) {
                if (result.state == 1) {
                    window.location.href = "${pageContext.request.contextPath}/Home/Index";
                    $(".btn-submit").html("登录成功，跳转中...");
                } else {
                    $(".btn-submit").html("立即登录");
                    $(".btn-submit").attr('disabled', false).removeClass('layui-disabled');
                    $("#verifyCode").trigger('click');
                    $.layerMsg(result.message, result.state);
                }
            }
        });
        return false;
    });
    $("#verifyCode").click(function () {
        $(this).attr("src", "${pageContext.request.contextPath}/Account/VerifyCode?r=" + Math.random());
    });
});
</script>
