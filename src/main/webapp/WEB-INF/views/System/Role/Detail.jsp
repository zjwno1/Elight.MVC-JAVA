<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/ico" href="${pageContext.request.contextPath}/favicon.ico" />
    <link rel="bookmark" type="image/ico" href="${pageContext.request.contextPath}/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link href="${pageContext.request.contextPath}/Content/layui/css/layui.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/select2/css/select2.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/framework/css/console.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/framework/css/animate.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <script src="${pageContext.request.contextPath}/Content/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/Content/layui/layui.js"></script>
    <script src="${pageContext.request.contextPath}/Content/select2/js/select2.min.js"></script>
    <script src="${pageContext.request.contextPath}/Content/framework/js/global.js"></script>
</head>

<form id="form" class="layui-form" style="margin-top: 25px">
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">编码</label>
            <div class="layui-input-inline">
                <input type="text" name="enCode" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">名称</label>
            <div class="layui-input-inline">
                <input type="text" name="name" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">类型</label>
            <div class="layui-input-inline">
                <select name="type">
                    <option value=""></option>
                    <option value="0">系统角色</option>
                    <option value="1">业务角色</option>
                    <option value="2">其他角色</option>
                </select>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">所属机构</label>
            <div class="layui-input-inline">
                <select lay-ignore class="select2" name="organizeId" id="organizeId" style="width: 190px;">
                </select>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">排序码</label>
            <div class="layui-input-inline">
                <input type="number" name="sortCode" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">选项</label>
            <div class="layui-input-block">
                <input type="checkbox" lay-skin="primary" name="isEnabled" value="true" title="启用" checked>
                <input type="checkbox" lay-skin="primary" name="allowEdit" value="true" title="允许编辑">
            </div>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">备注</label>
        <div class="layui-input-block" style="width: 514px;">
            <textarea name="remark" class="layui-textarea"></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">创建人员</label>
            <div class="layui-input-inline">
                <input type="text" name="createUser" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">创建时间</label>
            <div class="layui-input-inline">
                <input type="text" name="createTime" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">修改人员</label>
            <div class="layui-input-inline">
                <input type="text" name="modifyUser" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">修改时间</label>
            <div class="layui-input-inline">
                <input type="text" name="modifyTime" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
    </div>
</form>
<script>
    layui.use('form', function () {
        var form = layui.form;
        $("#organizeId").bindSelect({
            url: "${pageContext.request.contextPath}/System/Organize/GetListTreeSelect"
        });

        var primaryKey = $.getQueryString("primaryKey");
        $.ajax({
            url: "${pageContext.request.contextPath}/System/Role/GetForm",
            data: { primaryKey: primaryKey },
            type: "post",
            dataType: "json",
            async: false,
            success: function (data) {
                $("#form").formSerialize(data);
            }
        });

        form.render();
    });
</script>

</body>
</html>