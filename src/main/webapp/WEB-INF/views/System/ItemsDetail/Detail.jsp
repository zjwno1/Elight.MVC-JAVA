<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/ico" href="${pageContext.request.contextPath}/favicon.ico" />
    <link rel="bookmark" type="image/ico" href="${pageContext.request.contextPath}/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link href="${pageContext.request.contextPath}/Content/layui/css/layui.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/select2/css/select2.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/framework/css/console.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/framework/css/animate.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <script src="${pageContext.request.contextPath}/Content/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/Content/layui/layui.js"></script>
    <script src="${pageContext.request.contextPath}/Content/select2/js/select2.min.js"></script>
    <script src="${pageContext.request.contextPath}/Content/framework/js/global.js"></script>
</head>

<form id="form" class="layui-form" style="margin-top: 25px">
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">编码</label>
            <div class="layui-input-inline">
                <input type="hidden" name="id" />
                <input type="hidden" name="itemId" />
                <input type="text" name="enCode" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">选项名称</label>
            <div class="layui-input-inline">
                <input type="text" name="name" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
    </div>
     <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">颜色</label>
            <div class="layui-input-inline">
                <input type="text" name="color"  autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label"></label>
            <div class="layui-input-inline">
                
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">选项</label>
            <div class="layui-input-inline">
                <input type="checkbox" lay-skin="primary" name="isEnabled" value="true" title="启用" checked disabled />
                <input type="checkbox" lay-skin="primary" name="isDefault" value="true" title="默认值" disabled />
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">排序码</label>
            <div class="layui-input-inline">
                <input type="number" name="sortCode" lay-verify="number" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">创建人员</label>
            <div class="layui-input-inline">
                <input type="text" name="createUser" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">创建时间</label>
            <div class="layui-input-inline">
                <input type="text" name="createTime" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">修改人员</label>
            <div class="layui-input-inline">
                <input type="text" name="modifyUser" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">修改时间</label>
            <div class="layui-input-inline">
                <input type="text" name="modifyTime" autocomplete="off" class="layui-input" disabled />
            </div>
        </div>
    </div>
</form>
<script>
    layui.use(['form'], function () {
        var form = layui.form;

        var primaryKey = $.getQueryString("primaryKey");
        if (primaryKey) {
            $.ajax({
                url: "${pageContext.request.contextPath}/System/ItemsDetail/GetForm",
                data: { primaryKey: primaryKey },
                type: "post",
                dataType: "json",
                async: false,
                success: function (data) {
                    $("#form").formSerialize(data);
                }
            });
        }

        form.render();

    });
</script>
</body>
</html>