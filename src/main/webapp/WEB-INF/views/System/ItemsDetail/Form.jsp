<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/ico" href="${pageContext.request.contextPath}/Content/favicon.ico" />
    <link rel="bookmark" type="image/ico" href="${pageContext.request.contextPath}/Content/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link href="${pageContext.request.contextPath}/Content/layui/css/layui.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/select2/css/select2.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/framework/css/console.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/framework/css/animate.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/Content/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <script src="${pageContext.request.contextPath}/Content/jquery/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/Content/layui/layui.js"></script>
    <script src="${pageContext.request.contextPath}/Content/select2/js/select2.min.js"></script>
    <script src="${pageContext.request.contextPath}/Content/framework/js/global.js"></script>
</head>
<body>
<form id="form" class="layui-form" style="margin-top: 25px">
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">编码</label>
            <div class="layui-input-inline">
                <input type="hidden" name="id" />
                <input type="hidden" name="itemId" id="itemId" />
                <input type="text" name="enCode" lay-verify="required" placeholder="请输入编码" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">选项名称</label>
            <div class="layui-input-inline">
                <input type="text" name="name" lay-verify="required" placeholder="请输入名称" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
           <label class="layui-form-label"></label>
            <div class="layui-input-inline">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">排序码</label>
            <div class="layui-input-inline">
                <input type="number" name="sortCode" lay-verify="number" placeholder="请输入排序" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">选项</label>
            <div class="layui-input-inline">
                <input type="checkbox" lay-skin="primary" name="isEnabled" value="true" title="启用" checked>
                <input type="checkbox" lay-skin="primary" name="isDefault" value="true" title="默认值">
            </div>
        </div>
    </div>
    <div class="layui-form-item" style="display: none">
        <div class="layui-input-block">
            <button id="btnSubmit" class="layui-btn" lay-submit lay-filter="add">提交</button>
        </div>
    </div>
    
</form>
<script>
    layui.use(['form'], function () {
        var form = layui.form;

        var primaryKey = $.getQueryString("primaryKey");
        var itemId = $.getQueryString("itemId");
        if (primaryKey) {
            $.ajax({
                url: "${pageContext.request.contextPath}/System/ItemsDetail/GetForm",
                data: { primaryKey: primaryKey },
                type: "post",
                dataType: "json",
                async: false,
                success: function (data) {
                    $("#form").formSerialize(data);
                }
            });
        }
        if (itemId) {
            $('#itemId').val(itemId);
        }

        form.render();

        form.on('submit(add)', function (data) {
            $.formSubmit({
                url: "${pageContext.request.contextPath}/System/ItemsDetail/Form",
                data: data.field
            });
            return false;
        });
    });

</script>
</body>
</html>