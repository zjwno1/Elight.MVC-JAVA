# elight.mvc
一款基于Web的通用管理系统轻量级解决方案。
## 快速开发
* 开发环境：VS2012及以上版本
* 实验数据库：SQL Server 2008R2
## 系统说明
* Elight.MVC是一套基于 ASP.NET MVC5 + Layui开发的通用管理系统快速开发框架。
* 支持SQL Server、MySQL、PostgreSQL和Oracle等多种数据库类型。
* 该解决方案适用于OA、电商平台、CRM、物流管理、教务管理等各类管理系统开发。
* 兼容除IE8以下所有浏览器，暂不支持移动端。
* 初始用户名：admin 密码：123456
## 变更记录
* 2018年7月本人将其改至Java版本
* 数据库：MySQL5.6及以上版本
* 升级了LayUI版本 目前版本:2.2.6
* 修改了原来的一些bug
* 使用了Spring以及Mybatis框架
* 数据库DDL和DML 在src/main/resoures下
* 原始ASP.NET代码github：https://github.com/esofar/elight.mvc

* ueditor使用Demo 请访问 http://127.0.0.1:8080/ueditor
* excel导出demo 请访问 http://127.0.0.1:8080/export
